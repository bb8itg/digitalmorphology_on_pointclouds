L = imread('bricks.jpg');
treshold = L > 20;
L2 = imerode( L > 30, ones(2,10));
L2 = imerode( L > 30, ones(10, 2));
%opening
L2 = imerode(L2, ones(15,15));
L2 = imdilate(L2, ones(15,15));
figure, montage({treshold, L2});