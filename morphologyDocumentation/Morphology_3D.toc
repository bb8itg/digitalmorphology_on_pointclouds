\contentsline {section}{\numberline {1}Abstract}{3}{section.1}%
\contentsline {section}{\numberline {2}Introduction}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Example of morphology on an Image}{3}{subsection.2.1}%
\contentsline {section}{\numberline {3}Method}{5}{section.3}%
\contentsline {subsection}{\numberline {3.1}Morphological Operations in 2 Dimension}{5}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Dilation}{5}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}Erosion}{7}{subsubsection.3.1.2}%
\contentsline {subsection}{\numberline {3.2}Data Structure}{8}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Array of points}{8}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}Tree of Z, X, Y axis}{9}{subsubsection.3.2.2}%
\contentsline {subsubsection}{\numberline {3.2.3}Matrix hashmap of Trees of Z, X, Y axis}{10}{subsubsection.3.2.3}%
\contentsline {subsection}{\numberline {3.3}Kernels}{11}{subsection.3.3}%
\contentsline {subsubsection}{\numberline {3.3.1}Planes}{11}{subsubsection.3.3.1}%
\contentsline {subsubsection}{\numberline {3.3.2}Cylinders}{15}{subsubsection.3.3.2}%
\contentsline {subsubsection}{\numberline {3.3.3}Sphere}{19}{subsubsection.3.3.3}%
\contentsline {subsection}{\numberline {3.4}Algorithms}{22}{subsection.3.4}%
\contentsline {subsubsection}{\numberline {3.4.1}Dilation on Array of points}{23}{subsubsection.3.4.1}%
\contentsline {subsubsection}{\numberline {3.4.2}Dilation on Tree of Z, X, Y}{24}{subsubsection.3.4.2}%
\contentsline {subsubsection}{\numberline {3.4.3}Dilation on Matrix hashmap of Trees of Z, X, Y}{26}{subsubsection.3.4.3}%
\contentsline {subsubsection}{\numberline {3.4.4}Erosion on Array of points}{27}{subsubsection.3.4.4}%
\contentsline {subsubsection}{\numberline {3.4.5}Erosion on Tree of Z, X, Y}{28}{subsubsection.3.4.5}%
\contentsline {subsubsection}{\numberline {3.4.6}Erosion on Matrix hashmap of Trees of Z, X, Y}{30}{subsubsection.3.4.6}%
\contentsline {subsubsection}{\numberline {3.4.7}Searching for Point}{31}{subsubsection.3.4.7}%
\contentsline {subsubsection}{\numberline {3.4.8}Inserting Point}{33}{subsubsection.3.4.8}%
\contentsline {section}{\numberline {4} Results and Discussion}{34}{section.4}%
\contentsline {subsection}{\numberline {4.1}Detecting planes using Array of points}{34}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Detecting planes using Tree of Z, X, Y}{37}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Detecting planes using Matrix hashmap of Trees of Z, X, Y}{41}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Detecting cylinders}{44}{subsection.4.4}%
\contentsline {subsection}{\numberline {4.5}Detecting cylinders using Tree of Z, X, Y}{47}{subsection.4.5}%
\contentsline {subsection}{\numberline {4.6}Detecting cylinders using Matrix hashmap of Trees of Z, X, Y}{51}{subsection.4.6}%
\contentsline {subsection}{\numberline {4.7}Detecting Spheres}{51}{subsection.4.7}%
\contentsline {subsection}{\numberline {4.8}Detecting Spheres using Tree of Z, X, Y}{54}{subsection.4.8}%
\contentsline {subsection}{\numberline {4.9}Detecting Spheres using Matrix hashmap of Trees of Z, X, Y}{55}{subsection.4.9}%
\contentsline {subsection}{\numberline {4.10}Comparison with RANSAC and Hough Transform}{56}{subsection.4.10}%
\contentsline {subsubsection}{\numberline {4.10.1}RANSAC detect Planes}{56}{subsubsection.4.10.1}%
\contentsline {subsubsection}{\numberline {4.10.2}RANSAC detect Cylinders}{62}{subsubsection.4.10.2}%
\contentsline {subsubsection}{\numberline {4.10.3}RANSAC detect Spheres}{65}{subsubsection.4.10.3}%
\contentsline {subsubsection}{\numberline {4.10.4}Hough Transform detect Planes}{66}{subsubsection.4.10.4}%
\contentsline {subsection}{\numberline {4.11}Synthetic test}{71}{subsection.4.11}%
\contentsline {subsubsection}{\numberline {4.11.1}Generating a test field for detection}{71}{subsubsection.4.11.1}%
\contentsline {subsubsection}{\numberline {4.11.2}Planes}{76}{subsubsection.4.11.2}%
\contentsline {subsubsection}{\numberline {4.11.3}Planes with extreme noise}{78}{subsubsection.4.11.3}%
\contentsline {subsubsection}{\numberline {4.11.4}Cylinders}{79}{subsubsection.4.11.4}%
\contentsline {subsubsection}{\numberline {4.11.5}Cylinders with extreme noise}{80}{subsubsection.4.11.5}%
\contentsline {subsubsection}{\numberline {4.11.6}Spheres}{82}{subsubsection.4.11.6}%
\contentsline {subsubsection}{\numberline {4.11.7}Spheres with extreme noise}{83}{subsubsection.4.11.7}%
\contentsline {subsubsection}{\numberline {4.11.8}DBSCAN reference code}{84}{subsubsection.4.11.8}%
\contentsline {subsection}{\numberline {4.12}Overall Results}{86}{subsection.4.12}%
\contentsline {subsubsection}{\numberline {4.12.1}Histograms}{86}{subsubsection.4.12.1}%
\contentsline {subsubsection}{\numberline {4.12.2}Confusion Matrix}{89}{subsubsection.4.12.2}%
\contentsline {subsection}{\numberline {4.13}Testing Hardware and Software}{91}{subsection.4.13}%
\contentsline {subsubsection}{\numberline {4.13.1}Hardware}{91}{subsubsection.4.13.1}%
\contentsline {subsubsection}{\numberline {4.13.2}Software}{91}{subsubsection.4.13.2}%
\contentsline {section}{\numberline {5}Conclusion}{92}{section.5}%
\contentsline {subsection}{\numberline {5.1}Other possible applications}{92}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Improvements}{93}{subsection.5.2}%
