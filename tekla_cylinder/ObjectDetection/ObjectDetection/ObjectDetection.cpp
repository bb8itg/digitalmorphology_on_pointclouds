#include "MatrixReaderWriter.h"
#include "PlaneEstimation.h"
#include "CylinderEstimation.h"
#include "SphereEstimation.h"
#include "PLYWriter.h"
#include <opencv2/opencv.hpp>
#include <vector>
#include <string>

using namespace cv;

#define THERSHOLD 0.05  //RANSAC threshold (if Velodyne scans are processed, the unit is meter)

#define RANSAC_ITER 1000   //RANSAC iteration

#define FILTER_LOWEST_DISTANCE 0.3 //threshold for pre-filtering

int main(int argc, char** argv){
    
    if (argc!=4){
        printf("Usage:\nObjectDetection input.xyz output.ply objType\n");
        exit(EXIT_FAILURE);
        
    }
    
    MatrixReaderWriter mrw(argv[1]);
    
    int num=mrw.rowNum;
    
    cout<< "Rows:" << num <<endl;
    cout<< "Cols:" << mrw.columnNum << endl;

    // Read data from text file   
    vector<Point3f> points;
    
    for (int idx=0;idx<num;idx++){
       double x=mrw.data[3*idx];
       double y=mrw.data[3*idx+1];
       double z=mrw.data[3*idx+2];
       
       float distFromOrigo=sqrt(x*x+y*y+z*z);


       // First filter: minimal work distance for a LiDAR limited.        
       if (distFromOrigo>FILTER_LOWEST_DISTANCE){
           Point3f newPt;
           newPt.x=x;
           newPt.y=y;
           newPt.z=z;
           points.push_back(newPt);
           }
       
    }
    
    // Number of points:
    num=points.size();
    bool fitCylinder;
    string objType(argv[3]);
    if (objType == "cylinder") {
        fitCylinder = true;
    }
    else if (objType == "sphere") {
        fitCylinder = false;
    }
    else {
        printf("Usage:\nObjectDetection input.xyz output.ply objType\n");
        exit(EXIT_FAILURE);
    }
    RANSACDiffs differences;
    array<float, 4> params;

    if (fitCylinder) {
        // Vertical cylinder fitting
        // RANSAC-based robust cylinder estimation
        params = EstimateCylinderRANSAC(points, THERSHOLD, RANSAC_ITER);
        printf("Cylinder params RANSAC:\n px:%f py:%f pz:%f r:%f \n", params[0], params[1], params[2], params[3]);

        // Compute differences of the fitted cylinder in order to separate inliers from outliers
        differences = CylinderPointRANSACDifferences(points, params, THERSHOLD);
    }
    else {
        // Sphere cylinder fitting
        // RANSAC-based robust sphere estimation
        params = EstimateSphereRANSAC(points, THERSHOLD, RANSAC_ITER);
        printf("Sphere params RANSAC:\n px:%f py:%f pz:%f r:%f \n", params[0], params[1], params[2], params[3]);

        // Compute differences of the fitted sphere in order to separate inliers from outliers
        differences = SpherePointRANSACDifferences(points, params, THERSHOLD);
    }

    // Inliers and outliers are coloured by green and red, respectively
    vector<Point3i> colorsRANSAC;
    
    for (int idx=0;idx<num;idx++) {
        Point3i newColor;

        if (differences.isInliers.at(idx)) {
           newColor.x=0;
           newColor.y=255;
           newColor.z=0;
        } else {
           newColor.x=255;
           newColor.y=0;
           newColor.z=0;
        }
        colorsRANSAC.push_back(newColor);    
    }

    // Add the axis point of the cylinder / the sphere center with blue
    points.push_back(Point3f(params[0], params[1], params[2]));
    colorsRANSAC.push_back(Point3i(0, 0, 255));
    
    //Write results into a PLY file. 
    //It can be isualized by open-source 3D application Meshlab (www.meshlab.org)
    WritePLY(argv[2],points,colorsRANSAC);    
}
