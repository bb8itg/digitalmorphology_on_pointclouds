# include "test.h"
//gcc -D__USE_MINGW_ANSI_STDIO *.c
//g++ -D__USE_MINGW_ANSI_STDIO *.c -w
//nvcc -I cuda_playground/common/inc/ -D__USE_MINGW_ANSI_STDIO *.c
//pure C to cuda: https://forums.developer.nvidia.com/t/compiling-c-and-cuda-code-problems-linking-cuda-code-and-c-code/24787/4
int main(int argsNum, char* args[]) {

    //TEST_dilation_matrix_tree();
    //TEST_detect_all_planes_matrix_treeData();
    //VISUALIZE_cylinder_kernel();
    //VISUALIZE_cylinder_kernel_handTailored();
    //TEST_detect_cylinders_handTailored_treeData();
    //TEST_detect_cylinders_handTailored_matrix_treeData();
    //TEST_downsample_pointcloud_treeData();
    //TEST_detect_spheres_treeData();
    //TEST_detect_all_planes_matrix_treeData();
    //TEST_detect_cylinders_handTailored_treeData();
    //TEST_detect_X_Y_planes_arrayData();

    //VISUALIZE_sphere_kernel();


    TEST_detect_all_planes_matrix_treeData();
    //TEST_detect_cylinders_handTailored_matrix_treeData();
    //TEST_detect_cylinders_handTailored_treeData();
    //TEST_detect_spheres_matrix_treeData();
    return 0;
}
