#ifndef HANDLEFILE_3DPOINTCLOUD_H
#define HANDLEFILE_3DPOINTCLOUD_H
#include "morphologicalOperators.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


points_3D_set* handleXYZ_files(const char* fileName);
void saveXYZ_files(char* fileName, points_3D_set* points3D);
void saveXYZ_kernel_asFile(char* fileName, kernel*);
void saveXYZ_kernelSet_asFile(char* fileName, kernel** ker, int numOfKernels);
void saveXYZ_files_Tree(char* fileName, Node* head);
void node_file_Writer(FILE* ptr, Node* node, int*, points_3D_set* points3D);
#endif