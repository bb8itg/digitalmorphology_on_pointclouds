#ifndef MORPHOLOGICAL_OPERATORS_H
#define MORPHOLOGICAL_OPERATORS_H

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <thread>
#include "lidarFeatures.h"


#define BUFFER_SIZE 128
#define TEMPBUFFER_SIZE 128
//#define UNIONTRESHOLD (long double)0.05
#define UNIONTRESHOLD (long double)0.05
//g++ -D__USE_MINGW_ANSI_STDIO *.c


typedef struct{
    long double point[3];
} point_3D;

typedef struct{
    point_3D** points;
    unsigned int numberOfPoints;
    long double unionTreshold;
} points_3D_set;

typedef struct{
    point_3D** points;
    unsigned short numberOfPoints;
} kernel;


typedef struct Node{
    struct Node* x;
    struct Node* y;
    struct Node* z;
    point_3D* point;
} Node;


typedef struct MatrixTree{
    Node*** matrixValue;
    int** matrixElementSize;
    long double min_X, min_Y, max_X, max_Y;
    int dimX, dimY;
    long double space_X, space_Y;
    point_3D* point;
} MatrixTree;
// ================================ MORPHOLGY OPERATORS ================================
void dilate(points_3D_set* points3D, kernel* kernel);
void erode(points_3D_set* points3D, kernel* kernel, long double treshold);

Node* erode_Tree(Node* head, kernel* kernel, long double threshold);
void erode_Tree_iterate(Node* head, Node* newHead, Node* nextNode, kernel* kernel, long double threshold);
Node* dilate_Tree(Node* head, kernel* kernel, long double threshold);
void dilate_Tree_iterate(Node* head, Node* newHead, Node* nextNode, kernel* kernel, long double threshold);

MatrixTree* erode_Matrix_Tree(MatrixTree* matrixTree, MatrixTree* savedMatrixTree, kernel* kernel, long double threshold);
MatrixTree*  dilate_Matrix_Tree(MatrixTree* matrixTree, MatrixTree* savedMatrixTree, kernel* kernel, long double threshold);
// ================================ points_3D_set, point_3D FUNCTIONS ================================
void maintainSet(points_3D_set* points3D);
unsigned int comparePoints(point_3D* pt1, point_3D* pt2, long double treshold);
void copyPoints(point_3D* dest, point_3D* source);
point_3D* addPoint(point_3D p_1, point_3D p_2);
void cleanSet(points_3D_set* points3D);

// ================================ SET OPERATORS ================================
points_3D_set* intersection(points_3D_set*, points_3D_set*);
points_3D_set* setDifference(points_3D_set*, points_3D_set*);


Node* subtract_Tree(Node* subtract_from, Node* subtractor, long double threshold);
Node* union_Tree(Node* tree_set_1, Node* tree_set_2, long double threshold);
int sizeOfTree(Node* head);

// ================================ KERNEL GENERATION ================================
kernel* cubeKernel(unsigned int, long double );
kernel* square_Vertical_along_X(unsigned int, long double );
kernel* square_Vertical_along_Y(unsigned int, long double );
kernel* square_horizontal(unsigned int, long double );

kernel* verticalLineKernel(unsigned int k, long double spaceing);

kernel* horizontal_X_LineKernel_eqSpaced(unsigned int k, long double spaceing);
kernel* horizontal_X_LineKernel_lidarSpaced(unsigned int k, long double range_in_meter, LidarFeatures* ptr);

kernel* horizontal_Y_LineKernel_eqSpaced(unsigned int k, long double spaceing);
kernel* horizontal_Y_LineKernel_lidarSpaced(unsigned int k, long double range_in_meter, LidarFeatures* ptr);

kernel* horizontal_positive45Deg_LineKernel_lidarSpaced(unsigned int k, long double range_in_meter, LidarFeatures* ptr);
kernel* horizontal_negative45Deg_LineKernel_lidarSpaced(unsigned int k, long double range_in_meter, LidarFeatures* ptr);
kernel* horizontal_positiveDeg_LineKernel_lidarSpaced(unsigned int k, long double range_in_meter, long double rotDeg, LidarFeatures* ptr);


kernel* rotate3D_LineKernel_lidarSpaced(unsigned int k, long double range_in_meter,long double rotDeg_horizontal ,long double rotDeg_vertical, LidarFeatures* ptr);
kernel* rotate3D_LineKernel_eqSpaced(unsigned int k, long double range_in_meter,long double rotDeg_horizontal ,long double rotDeg_vertical);


kernel* horizontal_cylinder_kernel_eqSpaced(unsigned int k, long double c, long double spaceing, long double theta);
kernel* horizontal_cylinder_leveled_kernel_eqSpaced(unsigned int k, long double c, long double spacing, long double theta, long double level_spacing, int level);
kernel* vertical_cylinder_kernel_eqSpaced(unsigned int k, long double c, long double spacing, long double theta);
long double cylinderEquation(long double x, long double c);
kernel* sphere_kernel_eqSpaced(unsigned int k, unsigned int z, long double c, long double spacing, long double theta, long double level_spacing);

points_3D_set* visualizeKernel( kernel* (kernelFunc)(unsigned int, long double, LidarFeatures* ),  unsigned int k, LidarFeatures* ptr);

// ================================ DATA STRUCTURE ================================

MatrixTree* createMatrixTree(points_3D_set* points3D, int dimX, int dimY);
MatrixTree* createMatrixTree_empty(int dimX, int dimY, long double min_X, long double min_Y, long double max_X, long double max_Y);

Node* createBinaryTree(points_3D_set* points3D);
Node* createBinaryTree_empty();
int insertTreeNode(Node*, point_3D*, long double );
int searchTree(point_3D* searchPoint, Node* head, long double threshold);

void searchTree_Zaxis(point_3D* searchPoint, Node* nextNode, long double threshold,  int* found);
void searchTree_Xaxis(point_3D* searchPoint, Node* nextNode, long double threshold,  int* found);
void searchTree_Yaxis(point_3D* searchPoint, Node* nextNode, long double threshold, int* found);
int searchTree_Rec(point_3D* searchPoint, Node* head, long double threshold);

// ==================================== HELPER FUNCTIONS ======================
void subtract_Tree_iterate(Node* newHead, Node* subtract_from, Node* subtractor, long double treshold);
void union_Tree_iterate(Node* newHead, Node* tree_set, long double treshold);
void sizeOfTree_iterate(Node* next, int* size_tree);
void erode_Matrix_Tree_iterate(MatrixTree* matrixTree, MatrixTree* newMatrixTree, Node* nextNode, kernel* kernel, int matrix_I, int matrix_J,long double threshold);
void dilate_Matrix_Tree_iterate(MatrixTree* matrixTree, MatrixTree* newMatrixTree, Node* nextNode, kernel* kernel, int matrix_I, int matrix_J,long double threshold);
#endif