#include "handleFile_3DPointCloud.h"

points_3D_set* handleXYZ_files(const char* fileName){
    points_3D_set* points3D = (points_3D_set*)malloc(sizeof(points_3D_set));
    points3D->unionTreshold = UNIONTRESHOLD;
    FILE *ptr = fopen(fileName, "r");
    if(ptr == NULL){
        printf("Error opening XYZ file");
        return NULL;
    }

    //================================== get number of lines ================================== 
    char ch;
    unsigned int linesNum = 0, currentLine = 0;
    while((ch=fgetc(ptr))!=EOF){
        if(ch == '\n'){
            ++linesNum;
        }
    }
    points3D->numberOfPoints = linesNum;
    
    //================================== save data to HEAP ================================== 
    rewind(ptr);
    char buffer[BUFFER_SIZE];
    char tempValueBuffer[TEMPBUFFER_SIZE];
    points3D->points = (point_3D**) calloc(linesNum, sizeof(point_3D*));
    for(unsigned int i = 0; i < linesNum; i++){
        points3D->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }
    char *end;
    while( fgets(buffer, BUFFER_SIZE, ptr) != NULL){
        unsigned short bufferIndex, tempIndex=0, readValCount = 0;
        for(bufferIndex = 0; buffer[bufferIndex] != '\0'; ++bufferIndex){
            if(buffer[bufferIndex] != ' ' && buffer[bufferIndex] != '\n'){
                tempValueBuffer[tempIndex] = buffer[bufferIndex];
                ++tempIndex;
            } else{
    
                points3D->points[currentLine]->point[readValCount] = strtold(tempValueBuffer, &end);
                //printf("%.16Lf ", points3D->points[currentLine]->point[readValCount]);
                tempIndex = 0;
                ++readValCount;
                memset(tempValueBuffer, 0, TEMPBUFFER_SIZE);
                if(readValCount > 2){
                    //printf("%s %d\n", tempValueBuffer, currentLine);
                   // printf("%.16Lf\n", points3D->points[currentLine]->point[readValCount]);
                    //printf("%d\n", currentLine);
                    break;
                }
            }
        }
        memset(buffer, 0, BUFFER_SIZE);
        ++currentLine;
        
    }
    printf("\n\nfile data: lines:%d\n\n", linesNum);
    fclose(ptr);
    return points3D;
}


void saveXYZ_files(char* fileName, points_3D_set* points3D){
    FILE* ptr = fopen(fileName, "w");
    for(unsigned int i = 0; i < points3D->numberOfPoints; i++){
        //if( points3D->points[i]->point[0] > 0. + points3D->unionTreshold ||  points3D->points[i]->point[1] > 0. + points3D->unionTreshold || points3D->points[i]->point[2] > 0. + points3D->unionTreshold)
        fprintf(ptr, "%.16Lf %.16Lf %.16Lf \n", points3D->points[i]->point[0], points3D->points[i]->point[1], points3D->points[i]->point[2]);
    }
    fclose(ptr);
}

void saveXYZ_files_Tree(char* fileName, Node* head){
    FILE* ptr = fopen(fileName, "w");
    int test = 0;

    points_3D_set* points3D = (points_3D_set*)malloc(sizeof(points_3D_set));
    points3D->unionTreshold = UNIONTRESHOLD;
    //EXTREMLY DANGEROUS
    points3D->numberOfPoints = 65536;
    points3D->points = (point_3D**) calloc(points3D->numberOfPoints = 65536, sizeof(point_3D*));
    for(unsigned int i = 0; i < points3D->numberOfPoints; i++){
        points3D->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }

    
    node_file_Writer(ptr, head,&test, points3D);
    points3D->numberOfPoints = test;
    for(unsigned int i = 0; i < points3D->numberOfPoints; i++){
        //if( points3D->points[i]->point[0] > 0. + points3D->unionTreshold ||  points3D->points[i]->point[1] > 0. + points3D->unionTreshold || points3D->points[i]->point[2] > 0. + points3D->unionTreshold)
        fprintf(ptr, "%.16Lf %.16Lf %.16Lf \n", points3D->points[i]->point[0], points3D->points[i]->point[1], points3D->points[i]->point[2]);

    }
    fclose(ptr);
}

void node_file_Writer(FILE* ptr, Node* node, int *value, points_3D_set* points3D){
    if(node != NULL){
        copyPoints(points3D->points[*value], node->point);
        //fprintf(stdout, "%.16Lf %.16Lf %.16Lf \n", node->point[0], node->point[1], node->point[2]);
        *value+=1;
        //printf("%d\n",*value);
        node_file_Writer(ptr, node->z, value, points3D);
        node_file_Writer(ptr, node->x, value, points3D);
        node_file_Writer(ptr, node->y, value, points3D);
    }
}


void saveXYZ_kernel_asFile(char* fileName, kernel* ker){
    FILE* ptr = fopen(fileName, "w");
    for(unsigned int i = 0; i < ker->numberOfPoints; i++){
        //if( points3D->points[i]->point[0] > 0. + points3D->unionTreshold ||  points3D->points[i]->point[1] > 0. + points3D->unionTreshold || points3D->points[i]->point[2] > 0. + points3D->unionTreshold)
        fprintf(ptr, "%.16Lf %.16Lf %.16Lf \n", ker->points[i]->point[0], ker->points[i]->point[1], ker->points[i]->point[2]);
    }
    fclose(ptr);
}

void saveXYZ_kernelSet_asFile(char* fileName, kernel** ker, int numOfKernels){
    FILE* ptr = fopen(fileName, "w");
    for(unsigned int z = 0; z < numOfKernels; ++z){
        for(unsigned int i = 0; i < ker[z]->numberOfPoints; i++){
            //if( points3D->points[i]->point[0] > 0. + points3D->unionTreshold ||  points3D->points[i]->point[1] > 0. + points3D->unionTreshold || points3D->points[i]->point[2] > 0. + points3D->unionTreshold)
            fprintf(ptr, "%.16Lf %.16Lf %.16Lf \n", ker[z]->points[i]->point[0], ker[z]->points[i]->point[1], ker[z]->points[i]->point[2]);
        }
    }
    fclose(ptr);
}