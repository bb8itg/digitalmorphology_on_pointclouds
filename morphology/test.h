#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "handleFile_3DPointCloud.h"
#include "lidarFeatures.h"

//gcc -D__USE_MINGW_ANSI_STDIO *.c

//================= TESTING MORPHOLOGICAL OPERATORS =================
void TEST_dilation_tree();
void TEST_dilation_matrix_tree();
void TEST_downsample_pointcloud_treeData();

// ================= TESTING FUNCTIONS =================
void TEST_detect_X_Y_planes_arrayData();
void TEST_detect_45_deg_rotated_planes_arrayData();
void TEST_detect_cylinders_arrayData();

void TEST_detect_X_Y_45_deg_planes_treeData();
void TEST_detect_all_planes_treeData();
void TEST_detect_cylinders_treeData();
void TEST_detect_planes_cylinders_treeData();
void TEST_union_planes_cylinders_treeData();
void TEST_subtract_planes_treeData();
void TEST_detect_planes_veritcal_kernels_treeData();
void TEST_detect_cylinders_fineTuned_treeData();
void TEST_detect_cylinders_handTailored_treeData();


void TEST_create_matrix_treeData();
void TEST_detect_spheres_treeData();
void TEST_create_matrix_treeData_floor1();
void TEST_detect_planes_veritcal_kernels_treeData_matrix_floor1();
void TEST_detect_all_planes_matrix_treeData();
void TEST_detect_cylinders_handTailored_matrix_treeData();
void TEST_detect_spheres_matrix_treeData();

// ================= VISUALIZATION OF KERNEL FUNCTIONS =================
void VISUALIZE_horizontal_X_kernel();
void VISUALIZE_cylinder_kernel();
void VISUALIZE_cylinder_kernel_mini();
void VISUALIZE_cylinder_kernel_handTailored();
void VISUALIZE_sphere_kernel();
void VISUALIZE_rotated_vertical_kernel();


