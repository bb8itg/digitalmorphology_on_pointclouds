#include "lidarFeatures.h"

LidarFeatures* createLidarFeatures(int numberOfPoints, int vertical_lines){
    LidarFeatures* returnLidar = (LidarFeatures*) malloc(sizeof(LidarFeatures));
    returnLidar->vertical_lines = vertical_lines;
    returnLidar->horizontal_points = numberOfPoints/vertical_lines;
    returnLidar->horizontal_angle = 360.0 / ((long double) returnLidar->horizontal_points);
    return returnLidar; 
}

void printLidarFeatures(const LidarFeatures* ptr){
    printf("vertical_lines: %i,  horizontal_points: %i, horizontal_angle: %.16Lf\n", ptr->vertical_lines, ptr->horizontal_points, ptr->horizontal_angle);
}
//is it really meter?????
long double spacing_by_Lidar_features(const LidarFeatures* ptr, long double range_in_meter){
    return (range_in_meter * tanl(ptr->horizontal_angle));
}