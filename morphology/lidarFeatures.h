#ifndef LIDAR_FEATURES_H
#define LIDAR_FEATURES_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct{
    unsigned int horizontal_points;
    unsigned int vertical_lines;
    long double horizontal_angle;
} LidarFeatures;

LidarFeatures* createLidarFeatures(int numberOfPoints, int vertical_lines);
void printLidarFeatures(const LidarFeatures* ptr);
long double spacing_by_Lidar_features(const LidarFeatures*  ,long double range_in_meter);

#endif