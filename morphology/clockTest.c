//#define CLOCKTEST_FILE_MAIN
#ifdef CLOCKTEST_FILE_MAIN

#include <stdio.h>
#include <time.h>

int main() {
    clock_t start, end;
    double cpu_time_used;

    // Record the start time
    start = clock();

    // Perform some task here (e.g., a loop)
    int i;
    for (i = 0; i < 1000000; i++) {
        // Some dummy operation
    }

    // Record the end time
    end = clock();

    // Calculate the CPU time used
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    // Convert CPU time to milliseconds
    double milliseconds = cpu_time_used * 1000;

    printf("Time taken: %f milliseconds\n", milliseconds);

    return 0;
}
#endif