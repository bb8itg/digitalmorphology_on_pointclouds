#include "morphologicalOperators.h"
#include <math.h>
#include <vector>
#include <exception>

//======================================================================================
// ================================ MORPHOLGY OPERATORS ================================
//======================================================================================
void dilate(points_3D_set* points3D, kernel* kernel){

    points_3D_set* newPointCloud = (points_3D_set*)malloc(sizeof(points_3D_set));
    newPointCloud->unionTreshold = UNIONTRESHOLD;
    unsigned int numberOfPoints = points3D->numberOfPoints * kernel->numberOfPoints, currentNum = 0;
    newPointCloud->points = (point_3D**) calloc(numberOfPoints, sizeof(point_3D*));
    for(unsigned int i = 0; i < numberOfPoints; i++){
        newPointCloud->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }

    newPointCloud->numberOfPoints = numberOfPoints;
    for(unsigned int i = 0; i < points3D->numberOfPoints; ++i){
        for(unsigned int j = 0;  j < kernel->numberOfPoints; ++j){
            newPointCloud->points[currentNum]->point[0] = kernel->points[j]->point[0] + points3D->points[i]->point[0];
            newPointCloud->points[currentNum]->point[1] = kernel->points[j]->point[1] + points3D->points[i]->point[1];            
            newPointCloud->points[currentNum]->point[2] = kernel->points[j]->point[2] + points3D->points[i]->point[2];
            ++currentNum;
        }
    }
    cleanSet(points3D);
    *points3D = *newPointCloud;
}

void erode(points_3D_set* points3D, kernel* kernel, long double treshold){
    points_3D_set* newPointCloud = (points_3D_set*)malloc(sizeof(points_3D_set));
    newPointCloud->unionTreshold = treshold;

    point_3D* erodedPoints = (point_3D*)calloc(points3D->numberOfPoints,sizeof(point_3D));
    unsigned int erodedPointsCounter = 0;
    point_3D* pointsToSave[128];

    for(unsigned int i = 0; i < points3D->numberOfPoints-1; ++i){
        if( points3D->points[i]->point[0] < 0.001 && 
            points3D->points[i]->point[0] > -0.001 && 
            points3D->points[i]->point[1] < 0.001 && 
            points3D->points[i]->point[1] > -0.001 &&  
            points3D->points[i]->point[2] < 0.001 && 
            points3D->points[i]->point[2] > -0.001){
            continue;
        }
        unsigned int realSubsetIndicator = 1;
        for(unsigned int j = 0;  j < kernel->numberOfPoints; ++j){
            point_3D tempPoint;
            tempPoint.point[0] = points3D->points[i]->point[0] + kernel->points[j]->point[0];
            tempPoint.point[1] = points3D->points[i]->point[1] + kernel->points[j]->point[1];            
            tempPoint.point[2] = points3D->points[i]->point[2] + kernel->points[j]->point[2];

            int subsetIndicator = 0;
            for(unsigned int z = 0;  z < points3D->numberOfPoints; ++z){
                if(comparePoints(&tempPoint, points3D->points[z], newPointCloud->unionTreshold) == 0){
                    subsetIndicator = 1;
                    pointsToSave[j] = points3D->points[z];
                    break;
                }
            }
            if(subsetIndicator == 0){
                realSubsetIndicator = 0;
                break;
            }
        }
        if(realSubsetIndicator == 1){
            //printf("POINT COPY\n");
            /*
            for(unsigned int z = 0; z < kernel->numberOfPoints; ++z){
                copyPoints(&erodedPoints[erodedPointsCounter],pointsToSave[z]);
                pointsToSave[z]->point[0] = 0.0;
                pointsToSave[z]->point[1] = 0.0;
                pointsToSave[z]->point[2] = 0.0;
            }*/
            copyPoints(&erodedPoints[erodedPointsCounter],points3D->points[i]);
            ++erodedPointsCounter;
        }
        
    }

    newPointCloud->numberOfPoints = erodedPointsCounter;
    newPointCloud->points = (point_3D**) calloc(newPointCloud->numberOfPoints, sizeof(point_3D*));
    for(unsigned int i = 0; i < newPointCloud->numberOfPoints; i++){
        newPointCloud->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }
    for(unsigned int i = 0; i < newPointCloud->numberOfPoints; ++i){
        copyPoints(newPointCloud->points[i], &erodedPoints[i]);

    }
    printf("\n\n%d\n\n", points3D->numberOfPoints);
    free(erodedPoints);
    cleanSet(points3D);
    *points3D = *newPointCloud;
}


Node* erode_Tree(Node* head, kernel* kernel, long double threshold){
    Node* nextNode = head;
    Node* newHead = (Node*)malloc(sizeof(Node));
    newHead->point = (point_3D*)malloc(sizeof(point_3D));
    newHead->point->point[0] = head->point->point[0] ;
    newHead->point->point[1] = head->point->point[1] ;
    newHead->point->point[2] = head->point->point[2] ;
    newHead->x = NULL;
    newHead->y = NULL;
    newHead->z = NULL;
    erode_Tree_iterate(head, newHead, nextNode, kernel, threshold);
    return newHead;
}

void erode_Tree_iterate(Node* head, Node* newHead, Node* nextNode, kernel* kernel, long double threshold){
    if( nextNode != NULL){
        int subsetIndicator = 1;
        point_3D** pointArray = (point_3D**)malloc(kernel->numberOfPoints * sizeof(point_3D*));
        for(unsigned int i = 0;  i < kernel->numberOfPoints; ++i){
            pointArray[i] = (point_3D*)malloc(sizeof(point_3D));
        }
        for(unsigned int i = 0;  i < kernel->numberOfPoints; ++i){
            point_3D tempPoint;
            tempPoint.point[0] = nextNode->point->point[0] + kernel->points[i]->point[0];
            tempPoint.point[1] = nextNode->point->point[1] + kernel->points[i]->point[1];            
            tempPoint.point[2] = nextNode->point->point[2] + kernel->points[i]->point[2];
            if(searchTree(&tempPoint, head, threshold) == -1){
                subsetIndicator = 0;
                break;
            }
            copyPoints(pointArray[i] , &tempPoint);
        }
        if(subsetIndicator == 1){
            insertTreeNode(newHead, nextNode->point, threshold);
            /*
            for(unsigned int i = 0;  i < kernel->numberOfPoints; ++i){
                insertTreeNode(newHead, pointArray[i], threshold);
            }*/
        }
        erode_Tree_iterate(head, newHead, nextNode->z, kernel, threshold);
        erode_Tree_iterate(head, newHead, nextNode->x, kernel, threshold);
        erode_Tree_iterate(head, newHead, nextNode->y, kernel, threshold);
    }
}

Node* dilate_Tree(Node* head, kernel* kernel, long double threshold){
    Node* nextNode = head;
    Node* newHead = (Node*)malloc(sizeof(Node));
    newHead->point = (point_3D*)malloc(sizeof(point_3D));
    newHead->point->point[0] = head->point->point[0] ;
    newHead->point->point[1] = head->point->point[1] ;
    newHead->point->point[2] = head->point->point[2] ;
    newHead->x = NULL;
    newHead->y = NULL;
    newHead->z = NULL;
    dilate_Tree_iterate(head, newHead, nextNode, kernel, threshold);
    return newHead;
}

void dilate_Tree_iterate(Node* head, Node* newHead, Node* nextNode, kernel* kernel, long double threshold){
    if( nextNode != NULL){
        insertTreeNode(newHead, nextNode->point, threshold);
        for(unsigned int i = 0; i < kernel->numberOfPoints; ++i){
            point_3D* tmp_point = addPoint( *(nextNode->point), *(kernel->points[i]));
            insertTreeNode(newHead, tmp_point, threshold);
        }
        dilate_Tree_iterate(head, newHead, nextNode->z, kernel, threshold);
        dilate_Tree_iterate(head, newHead, nextNode->x, kernel, threshold);
        dilate_Tree_iterate(head, newHead, nextNode->y, kernel, threshold);
    }
}

MatrixTree* dilate_Matrix_Tree(MatrixTree* matrixTree, MatrixTree* savedMatrixTree, kernel* kernel, long double threshold){
    std::vector<std::thread> threads;
    for(int i = 0; i < matrixTree->dimX; i++){
        for(int j = 0; j < matrixTree->dimY; j++){
            if(matrixTree->matrixElementSize[i][j] != 1){
                threads.emplace_back(dilate_Matrix_Tree_iterate, matrixTree, savedMatrixTree, matrixTree->matrixValue[i][j], kernel, i, j, threshold);
                
            }
        }
    }
    
    for(int i = 0; i < threads.size(); ++i){
        threads[i].detach();
    }
    return savedMatrixTree;
}


void dilate_Matrix_Tree_iterate(MatrixTree* matrixTree, MatrixTree* newMatrixTree, Node* nextNode, kernel* kernel, int matrix_I, int matrix_J,long double threshold){
    if( nextNode != NULL){
        for(unsigned int i = 0; i < kernel->numberOfPoints; ++i){
            point_3D* tmp_point = addPoint( *(nextNode->point), *(kernel->points[i]));
            unsigned index_X, index_Y;
            for(index_X = 1; index_X < matrixTree->dimX; ++index_X){
                if( tmp_point->point[0]  < matrixTree->space_X * (long double)index_X  + matrixTree->min_X){
                    break;
                }
            }
            for(index_Y = 1; index_Y < matrixTree->dimY; ++index_Y){
                if( tmp_point->point[1] < matrixTree->space_Y * (long double)index_Y  + matrixTree->min_Y){        
                    break;
                }
            }
            insertTreeNode(newMatrixTree->matrixValue[index_X-1][index_Y-1], tmp_point, threshold);
        }
        dilate_Matrix_Tree_iterate(matrixTree, newMatrixTree, nextNode->z, kernel, matrix_I, matrix_J, threshold);
        dilate_Matrix_Tree_iterate(matrixTree, newMatrixTree, nextNode->x, kernel, matrix_I, matrix_J, threshold);
        dilate_Matrix_Tree_iterate(matrixTree, newMatrixTree, nextNode->y, kernel, matrix_I, matrix_J, threshold);
    }
}

MatrixTree* erode_Matrix_Tree(MatrixTree* matrixTree, MatrixTree* savedMatrixTree, kernel* kernel, long double threshold){
    std::vector<std::thread> threads;

    for(int i = 0; i < matrixTree->dimX; i++){
        for(int j = 0; j < matrixTree->dimY; j++){
            if(matrixTree->matrixElementSize[i][j] != 1){
                threads.emplace_back(erode_Matrix_Tree_iterate, matrixTree, savedMatrixTree, matrixTree->matrixValue[i][j], kernel, i, j, threshold);
                //erode_Matrix_Tree_iterate(matrixTree, savedMatrixTree, matrixTree->matrixValue[i][j], kernel, i, j, threshold);
            }
        }
    }
    for(int i = 0; i < threads.size(); ++i){
        threads[i].join();
    }
    return savedMatrixTree;
}

void erode_Matrix_Tree_iterate(MatrixTree* matrixTree, MatrixTree* newMatrixTree, Node* nextNode, kernel* kernel, int matrix_I, int matrix_J,long double threshold){

    if( nextNode != NULL){
        unsigned index_X, index_Y;
        int subsetIndicator = 1;
        point_3D** pointArray = (point_3D**)malloc(kernel->numberOfPoints * sizeof(point_3D*));
        for(unsigned int i = 0;  i < kernel->numberOfPoints; ++i){
            pointArray[i] = (point_3D*)malloc(sizeof(point_3D));
        }
        for(unsigned int i = 0;  i < kernel->numberOfPoints; ++i){
            point_3D tempPoint;
            tempPoint.point[0] = nextNode->point->point[0] + kernel->points[i]->point[0];
            tempPoint.point[1] = nextNode->point->point[1] + kernel->points[i]->point[1];            
            tempPoint.point[2] = nextNode->point->point[2] + kernel->points[i]->point[2];

            // ==== Look for which tree to search ====
            for(index_X = 1; index_X < matrixTree->dimX; ++index_X){
                if( tempPoint.point[0]  < matrixTree->space_X * (long double)index_X  + matrixTree->min_X){
                    break;
                }
            }
            for(index_Y = 1; index_Y < matrixTree->dimY; ++index_Y){

                if( tempPoint.point[1] < matrixTree->space_Y * (long double)index_Y  + matrixTree->min_Y){        
                    break;
                }

            }
            if(searchTree(&tempPoint, matrixTree->matrixValue[index_X-1][index_Y-1], threshold) == -1){
                subsetIndicator = 0;
                break;
            }
            copyPoints(pointArray[i] , &tempPoint);
        }
        if(subsetIndicator == 1){
            insertTreeNode(newMatrixTree->matrixValue[matrix_I][matrix_J],  nextNode->point, threshold);
            newMatrixTree->matrixElementSize[matrix_I][matrix_J] += 1;
            //insertTreeNode(newHead, nextNode->point, threshold);
            /*
            for(unsigned int i = 0;  i < kernel->numberOfPoints; ++i){
                insertTreeNode(newHead, pointArray[i], threshold);
            }*/
        }
        erode_Matrix_Tree_iterate(matrixTree, newMatrixTree, nextNode->z, kernel, matrix_I, matrix_J, threshold);
        erode_Matrix_Tree_iterate(matrixTree, newMatrixTree, nextNode->x, kernel, matrix_I, matrix_J, threshold);
        erode_Matrix_Tree_iterate(matrixTree, newMatrixTree, nextNode->y, kernel, matrix_I, matrix_J, threshold);
    }

}

//================================================================================
// ================================ SET OPERATORS ================================
//================================================================================
points_3D_set* setDifference(points_3D_set* set1, points_3D_set* set2){
    points_3D_set* newPointCloud = (points_3D_set*)malloc(sizeof(points_3D_set));
    newPointCloud->unionTreshold = UNIONTRESHOLD;
    unsigned int pointCount = 0;
    point_3D** tempSet;

    if(set1->numberOfPoints < set2->numberOfPoints){
        tempSet = (point_3D**)calloc(set1->numberOfPoints*set2->numberOfPoints, sizeof(point_3D*));
        for(unsigned int i = 0; i < set1->numberOfPoints*set2->numberOfPoints; ++i){
            tempSet[i] = (point_3D*)malloc(sizeof(point_3D));
        }
    } else{
        tempSet = (point_3D**)calloc(set2->numberOfPoints*set2->numberOfPoints, sizeof(point_3D*));
        for(unsigned int i = 0; i < set2->numberOfPoints*set2->numberOfPoints; ++i){
            tempSet[i] = (point_3D*)malloc(sizeof(point_3D));
        }
    }

    for(unsigned int i = 0; i < set1->numberOfPoints; ++i){
        unsigned int isInSet = 1;
        for(unsigned int j = 0; j < set2->numberOfPoints; ++j){
            if(comparePoints(set1->points[i], set2->points[j], newPointCloud->unionTreshold)  == 0){
                isInSet = 0;
                break;
            }
        }
        if(isInSet){
            copyPoints(tempSet[pointCount], set1->points[i]);
            ++pointCount;
        }
    }

    newPointCloud->points = (point_3D**)calloc(pointCount, sizeof(point_3D*));
    newPointCloud->numberOfPoints = pointCount;


    for(unsigned int i = 0; i < pointCount; ++i){

        newPointCloud->points[i] = (point_3D*)malloc(sizeof(point_3D));
        copyPoints(newPointCloud->points[i], tempSet[i]);
    }

    for(unsigned int i = 0; i < pointCount; ++i){
        free(tempSet[i]);
    }
    free(tempSet);
    return newPointCloud;
}


points_3D_set* intersection(points_3D_set* set1, points_3D_set* set2){
    points_3D_set* newPointCloud = (points_3D_set*)malloc(sizeof(points_3D_set));
    newPointCloud->unionTreshold = UNIONTRESHOLD;
    unsigned int pointCount = 0;
    point_3D** tempSet;

    if(set1->numberOfPoints < set2->numberOfPoints){
        tempSet = (point_3D**)calloc(set1->numberOfPoints*set2->numberOfPoints, sizeof(point_3D*));
        for(unsigned int i = 0; i < set1->numberOfPoints*set2->numberOfPoints; ++i){
            tempSet[i] = (point_3D*)malloc(sizeof(point_3D));
        }
    } else{
        tempSet = (point_3D**)calloc(set2->numberOfPoints*set2->numberOfPoints, sizeof(point_3D*));
        for(unsigned int i = 0; i < set2->numberOfPoints*set2->numberOfPoints; ++i){
            tempSet[i] = (point_3D*)malloc(sizeof(point_3D));
        }
    }

    for(unsigned int i = 0; i < set1->numberOfPoints; ++i){
        for(unsigned int j = 0; j < set2->numberOfPoints; ++j){
            if(comparePoints(set1->points[i], set2->points[j], newPointCloud->unionTreshold)  == 0){
                copyPoints(tempSet[pointCount], set2->points[j]);
                ++pointCount;

                break;
            }
        }
    }

    newPointCloud->points = (point_3D**)calloc(pointCount, sizeof(point_3D*));
    newPointCloud->numberOfPoints = pointCount;


    for(unsigned int i = 0; i < pointCount; ++i){

        newPointCloud->points[i] = (point_3D*)malloc(sizeof(point_3D));
        copyPoints(newPointCloud->points[i], tempSet[i]);
    }

    for(unsigned int i = 0; i < pointCount; ++i){
        free(tempSet[i]);
    }
    free(tempSet);
    return newPointCloud;
}

Node* subtract_Tree(Node* subtract_from, Node* subtractor, long double threshold){
    Node* newHead = (Node*)malloc(sizeof(Node));
    newHead->point = (point_3D*)malloc(sizeof(point_3D));
    newHead->point->point[0] = subtract_from->point->point[0] ;
    newHead->point->point[1] = subtract_from->point->point[1] ;
    newHead->point->point[2] = subtract_from->point->point[2] ;
    newHead->x = NULL;
    newHead->y = NULL;
    newHead->z = NULL;
    subtract_Tree_iterate(newHead, subtract_from, subtractor, threshold);
    return newHead;
}

void subtract_Tree_iterate(Node* newHead, Node* subtract_from, Node* subtractor, long double treshold){
    if( subtract_from != NULL){
        //printf("%d\n", searchTree(subtract_from->point, subtractor, treshold));
        if( searchTree(subtract_from->point, subtractor, 0.000000000000001) == -1){
            insertTreeNode(newHead, subtract_from->point, 0.0001);
        }
        subtract_Tree_iterate(newHead, subtract_from->z, subtractor, treshold);
        subtract_Tree_iterate(newHead, subtract_from->x, subtractor, treshold);
        subtract_Tree_iterate(newHead, subtract_from->y, subtractor, treshold);
        
    }
}


Node* union_Tree(Node* tree_set_1, Node* tree_set_2, long double threshold){
    Node* newHead = (Node*)malloc(sizeof(Node));
    newHead->point = (point_3D*)malloc(sizeof(point_3D));
    newHead->point->point[0] = tree_set_1->point->point[0] ;
    newHead->point->point[1] = tree_set_1->point->point[1] ;
    newHead->point->point[2] = tree_set_1->point->point[2] ;
    newHead->x = NULL;
    newHead->y = NULL;
    newHead->z = NULL;
    union_Tree_iterate(newHead, tree_set_1, threshold);
    union_Tree_iterate(newHead, tree_set_2, threshold);
    return newHead;
}

void union_Tree_iterate(Node* newHead, Node* tree_set, long double treshold){
    if( tree_set != NULL){
        insertTreeNode(newHead, tree_set->point, treshold);

        union_Tree_iterate(newHead, tree_set->z, treshold);
        union_Tree_iterate(newHead, tree_set->x, treshold);
        union_Tree_iterate(newHead, tree_set->y, treshold);
    }
}

int sizeOfTree(Node* head){
    int size_tree = 0;
    sizeOfTree_iterate(head, &size_tree);
    return size_tree;
}

void sizeOfTree_iterate(Node* next, int* size_tree){
    if( next != NULL){
        *size_tree += 1;
        sizeOfTree_iterate(next->z, size_tree);
        sizeOfTree_iterate(next->x, size_tree);
        sizeOfTree_iterate(next->y, size_tree);
    }
}

//=====================================================================================================
// ================================  points_3D_set, point_3D FUNCTIONS ================================
//=====================================================================================================
void cleanSet(points_3D_set* points3D){
    for(unsigned int i = 0; i < points3D->numberOfPoints; ++i){
        free(points3D->points[i]->point);
    }
    free(points3D->points);
    points3D->numberOfPoints = 0;
    points3D->unionTreshold = 0.;

}

void maintainSet(points_3D_set* points3D){
    points_3D_set* newPointCloud = (points_3D_set*)malloc(sizeof(points_3D_set));
    unsigned int numberOfPoints, currentPoint = 0, found = 0;
    for(unsigned int i = 0; i < points3D->numberOfPoints-1; ++i){
        for(unsigned int j = i+1; j < points3D->numberOfPoints; ++j){
            if( points3D->points[j]->point[0] != 0. && points3D->points[j]->point[1] != 0. && points3D->points[j]->point[2] != 0.){
                if( comparePoints( points3D->points[i] ,  points3D->points[j], newPointCloud->unionTreshold) == 0){
                    points3D->points[j]->point[0] = 0.;
                    points3D->points[j]->point[1] = 0.;
                    points3D->points[j]->point[2] = 0.;
                //printf("DELETE");
                }
            }
        }
    }

    for(unsigned int i = 0; i < points3D->numberOfPoints; ++i){
        if( points3D->points[i]->point[0] > 0. + points3D->unionTreshold ||  points3D->points[i]->point[1] > 0. + points3D->unionTreshold || points3D->points[i]->point[2] > 0. + points3D->unionTreshold){
            ++numberOfPoints;
        }
    }

    newPointCloud->points = (point_3D**) calloc(numberOfPoints, sizeof(point_3D*));
    newPointCloud->numberOfPoints = numberOfPoints;
    for(unsigned int i = 0; i < numberOfPoints; i++){
        newPointCloud->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }

    for(unsigned int i = 0; i < points3D->numberOfPoints; ++i){
        if( points3D->points[i]->point[0] > 0. + points3D->unionTreshold ||  points3D->points[i]->point[1] > 0. + points3D->unionTreshold || points3D->points[i]->point[2] > 0. + points3D->unionTreshold){
            copyPoints(newPointCloud->points[currentPoint], points3D->points[i]);
            ++currentPoint;
        }
        
    }

    cleanSet(points3D);
    *points3D = *newPointCloud;
}
// TODO: look dangerous because of minus value minus minus is plus
unsigned int comparePoints(point_3D* pt1, point_3D* pt2, long double treshold){
    if( fabsl(pt1->point[0] - pt2->point[0]) < treshold &&
        fabsl(pt1->point[1] - pt2->point[1]) < treshold &&
        fabsl(pt1->point[2] - pt2->point[2]) < treshold 
    ){
        /*
        printf("%.16Lf \np1 (x: %.16Lf y: %.16Lf z: %.16Lf), p2() x: %.16Lf y: %.16Lf z: %.16Lf)\n", UNIONTRESHOLD,pt1->point[0], pt1->point[1], pt1->point[2], pt2->point[0], pt2->point[1], pt2->point[2]);
        printf("==== Compare ====\n");
        printf("x: %.16Lf\n", fabsl( pt1->point[0] - pt2->point[0]) );
        printf("x: %.16Lf\n", fabsl(pt1->point[1] - pt2->point[1]) );
        printf("x: %.16Lf\n\n", fabsl(pt1->point[2] - pt2->point[2]) );
        */
        return 0;
    }
    return 1;
}

void copyPoints(point_3D* dest, point_3D* source){
    dest->point[0] = source->point[0];
    dest->point[1] = source->point[1];
    dest->point[2] = source->point[2];
}

point_3D* addPoint(point_3D p_1, point_3D p_2){
    point_3D* pointSum = (point_3D*)malloc(sizeof(point_3D));
    pointSum->point[0] = p_1.point[0] + p_2.point[0];
    pointSum->point[1] = p_1.point[1] + p_2.point[1];
    pointSum->point[2] = p_1.point[2] + p_2.point[2];
    return pointSum;
}





//====================================================================================
// ================================ KERNEL GENERATION ================================
//====================================================================================
kernel * horizontal_X_LineKernel_eqSpaced(unsigned int k, long double spaceing){
    //make k ODD
    if(k % 2 == 0){
        --k;
    }
    kernel* ker = (kernel*)malloc(sizeof(kernel));
    ker->numberOfPoints = k;
    ker->points = (point_3D**)calloc(k, sizeof(point_3D*));
    for(unsigned int i=0; i < k; ++i){
        ker->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }
    ker->points[k/2]->point[0] = 0.;
    ker->points[k/2]->point[1] = 0.;
    ker->points[k/2]->point[2] = 0.;
    for(unsigned int i = 1; i <= k/2 ; ++i){
        ker->points[k/2-i]->point[0] = ker->points[k/2-i+1]->point[0] - spaceing;
        ker->points[k/2-i]->point[1] = ker->points[k/2-i+1]->point[1];
        ker->points[k/2-i]->point[2] = ker->points[k/2-i+1]->point[2];

        ker->points[k/2+i]->point[0] = ker->points[k/2+i-1]->point[0] + spaceing;
        ker->points[k/2+i]->point[1] = ker->points[k/2+i-1]->point[1];
        ker->points[k/2+i]->point[2] = ker->points[k/2+i-1]->point[2];
    }

    for(unsigned int i=0; i < k; ++i){
        printf("(%.16Lf %.16Lf %.16Lf)\n", ker->points[i]->point[0], ker->points[i]->point[1], ker->points[i]->point[2]);
    }
    return ker;
}

kernel* horizontal_X_LineKernel_lidarSpaced(unsigned int k, long double range_in_meter, LidarFeatures* ptr){
    if(k % 2 == 0){
        --k;
    }
    kernel* ker = (kernel*)malloc(sizeof(kernel));
    ker->numberOfPoints = k;
    ker->points = (point_3D**)calloc(k, sizeof(point_3D*));
    for(unsigned int i=0; i < k; ++i){
        ker->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }
    ker->points[k/2]->point[0] = 0.;
    ker->points[k/2]->point[1] = 0.;
    ker->points[k/2]->point[2] = 0.;
    long double spaceing, prevSpace = 0.0;
    for(unsigned int i = 1; i <= k/2 ; ++i){
        spaceing = range_in_meter * tanl(ptr->horizontal_angle*((long double)i))/* - prevSpace*/;
        ker->points[k/2-i]->point[0] = (-1.0)*(fabsl(ker->points[k/2-i+1]->point[0]) + spaceing - prevSpace);
        ker->points[k/2-i]->point[1] = ker->points[k/2-i+1]->point[1];
        ker->points[k/2-i]->point[2] = ker->points[k/2-i+1]->point[2];

        ker->points[k/2+i]->point[0] = ker->points[k/2+i-1]->point[0] + spaceing - prevSpace;
        ker->points[k/2+i]->point[1] = ker->points[k/2+i-1]->point[1];
        ker->points[k/2+i]->point[2] = ker->points[k/2+i-1]->point[2];
        prevSpace = spaceing;
    }

    for(unsigned int i=0; i < k; ++i){
        printf("(%.16Lf %.16Lf %.16Lf)\n", ker->points[i]->point[0], ker->points[i]->point[1], ker->points[i]->point[2]);
    }
    return ker;
}


kernel * horizontal_Y_LineKernel_eqSpaced(unsigned int k, long double spaceing){
    //make k ODD
    if(k % 2 == 0){
        --k;
    }
    kernel* ker = (kernel*)malloc(sizeof(kernel));
    ker->numberOfPoints = k;
    ker->points = (point_3D**)calloc(k, sizeof(point_3D*));
    for(unsigned int i=0; i < k; ++i){
        ker->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }
    ker->points[k/2]->point[0] = 0.;
    ker->points[k/2]->point[1] = 0.;
    ker->points[k/2]->point[2] = 0.;
    for(unsigned int i = 1; i <= k/2 ; ++i){
        ker->points[k/2-i]->point[0] = ker->points[k/2-i+1]->point[0];
        ker->points[k/2-i]->point[1] = (-1.0)*(fabsl(ker->points[k/2-i+1]->point[1]) + spaceing);
        ker->points[k/2-i]->point[2] = ker->points[k/2-i+1]->point[2];

        ker->points[k/2+i]->point[0] = ker->points[k/2+i-1]->point[0];
        ker->points[k/2+i]->point[1] = ker->points[k/2+i-1]->point[1] + spaceing;
        ker->points[k/2+i]->point[2] = ker->points[k/2+i-1]->point[2];
    }
/*
    for(unsigned int i=0; i < k; ++i){
        printf("(%.16Lf %.16Lf %.16Lf), ", ker->points[i]->point[0], ker->points[i]->point[1], ker->points[i]->point[2]);
    }
    */
    return ker;
}


kernel* horizontal_Y_LineKernel_lidarSpaced(unsigned int k, long double range_in_meter, LidarFeatures* ptr){
    if(k % 2 == 0){
        --k;
    }
    kernel* ker = (kernel*)malloc(sizeof(kernel));
    ker->numberOfPoints = k;
    ker->points = (point_3D**)calloc(k, sizeof(point_3D*));
    for(unsigned int i=0; i < k; ++i){
        ker->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }
    ker->points[k/2]->point[0] = 0.;
    ker->points[k/2]->point[1] = 0.;
    ker->points[k/2]->point[2] = 0.;
    long double spaceing, prevSpace = 0.0;
    for(unsigned int i = 1; i <= k/2 ; ++i){
        spaceing = range_in_meter * tanl(ptr->horizontal_angle*((long double)i)) - prevSpace;
        ker->points[k/2-i]->point[0] = ker->points[k/2-i+1]->point[0];
        ker->points[k/2-i]->point[1] = (-1.0)*(fabsl(ker->points[k/2-i+1]->point[1]) + spaceing - prevSpace);
        ker->points[k/2-i]->point[2] = ker->points[k/2-i+1]->point[2];

        ker->points[k/2+i]->point[0] = ker->points[k/2+i-1]->point[0];
        ker->points[k/2+i]->point[1] = ker->points[k/2+i-1]->point[1] + spaceing - prevSpace;
        ker->points[k/2+i]->point[2] = ker->points[k/2+i-1]->point[2];
        prevSpace = spaceing;
    }

    for(unsigned int i=0; i < k; ++i){
        //printf("(%.16Lf %.16Lf %.16Lf)\n", ker->points[i]->point[0], ker->points[i]->point[1], ker->points[i]->point[2]);
    }
    return ker;
}


kernel* horizontal_positive45Deg_LineKernel_lidarSpaced(unsigned int k, long double range_in_meter, LidarFeatures* ptr){
    if(k % 2 == 0){
        --k;
    }
    kernel* ker = (kernel*)malloc(sizeof(kernel));
    ker->numberOfPoints = k;
    ker->points = (point_3D**)calloc(k, sizeof(point_3D*));
    for(unsigned int i=0; i < k; ++i){
        ker->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }
    ker->points[k/2]->point[0] = 0.;
    ker->points[k/2]->point[1] = 0.;
    ker->points[k/2]->point[2] = 0.;
    long double spaceing_y, spaceing_x, prevSpace_x = 0.0, prevSpace_y = 0.0, z;
        for(unsigned int i = 1; i <= k/2 ; ++i){
        z = range_in_meter * tanl(ptr->horizontal_angle*((long double)i));
        spaceing_x = z*sinl(45) - prevSpace_x;
        spaceing_y = z*cosl(45) - prevSpace_y;
        ker->points[k/2-i]->point[0] = (-1.0)*(fabsl(ker->points[k/2-i+1]->point[0]) + spaceing_x - prevSpace_x);
        ker->points[k/2-i]->point[1] = (-1.0)*(fabsl(ker->points[k/2-i+1]->point[1]) + spaceing_y - prevSpace_y);
        ker->points[k/2-i]->point[2] = ker->points[k/2-i+1]->point[2];

        ker->points[k/2+i]->point[0] = ker->points[k/2+i-1]->point[0] + spaceing_x - prevSpace_x;
        ker->points[k/2+i]->point[1] = ker->points[k/2+i-1]->point[1] + spaceing_y - prevSpace_y;
        ker->points[k/2+i]->point[2] = ker->points[k/2+i-1]->point[2];
        prevSpace_y = spaceing_y;
        prevSpace_x = spaceing_x;
    }

    for(unsigned int i=0; i < k; ++i){
        printf("(%.16Lf %.16Lf %.16Lf)\n", ker->points[i]->point[0], ker->points[i]->point[1], ker->points[i]->point[2]);
    }
    return ker;
}

// deg 0 is y axis
kernel* horizontal_positiveDeg_LineKernel_lidarSpaced(unsigned int k, long double range_in_meter, long double rotDeg, LidarFeatures* ptr){
    
    long double rotateM[2][2] = {
        {cosl(rotDeg),-sinl(rotDeg)},
        {sinl(rotDeg),cosl(rotDeg)}
    };
    long double tempsaveXpoint, tempsaveYpoint;
    
    kernel* ker =  horizontal_Y_LineKernel_lidarSpaced(k, range_in_meter, ptr);
    for(unsigned int i = 0; i < ker->numberOfPoints; ++i){
        tempsaveXpoint = ker->points[i]->point[0];
        tempsaveYpoint = ker->points[i]->point[1];
        ker->points[i]->point[0] = rotateM[0][0] * tempsaveXpoint + rotateM[0][1] * tempsaveYpoint;
        ker->points[i]->point[1] = rotateM[1][0] * tempsaveXpoint + rotateM[1][1] * tempsaveYpoint;
        ker->points[i]->point[2] = ker->points[i]->point[2];
    }

    for(unsigned int i=0; i < k; ++i){
        printf("(%.16Lf %.16Lf %.16Lf)\n", ker->points[i]->point[0], ker->points[i]->point[1], ker->points[i]->point[2]);
    }
    return ker;
}

kernel* rotate3D_LineKernel_lidarSpaced(unsigned int k, long double range_in_meter,long double rotDeg_horizontal ,long double rotDeg_vertical, LidarFeatures* ptr){
    

    long double rotate_horizontal[2][2] = {
        {cosl(rotDeg_horizontal),-sinl(rotDeg_horizontal)},
        {sinl(rotDeg_horizontal),cosl(rotDeg_horizontal)}
    };

    long double rotate_vertical[2][2] = {
        {cosl(rotDeg_vertical),-sinl(rotDeg_vertical)},
        {sinl(rotDeg_vertical),cosl(rotDeg_vertical)}
    };
    long double rotation3D_matrix[3][3] = {
        { cosl(rotDeg_horizontal), -cosl(rotDeg_vertical)*sinl(rotDeg_horizontal),  sinl(rotDeg_horizontal)*sinl(rotDeg_vertical)},
        { sinl(rotDeg_horizontal), cosl(rotDeg_horizontal)*cosl(rotDeg_vertical), -cosl(rotDeg_horizontal) * sinl(rotDeg_vertical)},
        { 0                     , sinl(rotDeg_vertical)                          ,  cosl(rotDeg_vertical)}
    };

    long double tempsaveXpoint, tempsaveYpoint, tempsaveZpoint;
    
    kernel* ker =  horizontal_Y_LineKernel_lidarSpaced(k, range_in_meter, ptr);
    for(unsigned int i = 0; i < ker->numberOfPoints; ++i){
        tempsaveXpoint = ker->points[i]->point[0];
        tempsaveYpoint = ker->points[i]->point[1];
        tempsaveZpoint = ker->points[i]->point[2];

        ker->points[i]->point[0] = rotation3D_matrix[0][0] * tempsaveXpoint + rotation3D_matrix[0][1] * tempsaveYpoint + rotation3D_matrix[0][2] * tempsaveZpoint;
        ker->points[i]->point[1] = rotation3D_matrix[1][0] * tempsaveXpoint + rotation3D_matrix[1][1] * tempsaveYpoint + rotation3D_matrix[1][2] * tempsaveZpoint;
        ker->points[i]->point[2] = rotation3D_matrix[2][0] * tempsaveXpoint + rotation3D_matrix[2][1] * tempsaveYpoint + rotation3D_matrix[2][2] * tempsaveZpoint;
       
    }

    for(unsigned int i=0; i < k; ++i){
        printf("(%.16Lf %.16Lf %.16Lf)\n", ker->points[i]->point[0], ker->points[i]->point[1], ker->points[i]->point[2]);
    }
    return ker;
}

kernel* rotate3D_LineKernel_eqSpaced(unsigned int k, long double range_in_meter,long double rotDeg_horizontal ,long double rotDeg_vertical){
    

    long double rotate_horizontal[2][2] = {
        {cosl(rotDeg_horizontal),-sinl(rotDeg_horizontal)},
        {sinl(rotDeg_horizontal),cosl(rotDeg_horizontal)}
    };

    long double rotate_vertical[2][2] = {
        {cosl(rotDeg_vertical),-sinl(rotDeg_vertical)},
        {sinl(rotDeg_vertical),cosl(rotDeg_vertical)}
    };
    long double rotation3D_matrix[3][3] = {
        { cosl(rotDeg_horizontal), -cosl(rotDeg_vertical)*sinl(rotDeg_horizontal),  sinl(rotDeg_horizontal)*sinl(rotDeg_vertical)},
        { sinl(rotDeg_horizontal), cosl(rotDeg_horizontal)*cosl(rotDeg_vertical), -cosl(rotDeg_horizontal) * sinl(rotDeg_vertical)},
        { 0                     , sinl(rotDeg_vertical)                          ,  cosl(rotDeg_vertical)}
    };

    long double tempsaveXpoint, tempsaveYpoint, tempsaveZpoint;
    
    kernel* ker =  horizontal_Y_LineKernel_eqSpaced(k, range_in_meter);
    for(unsigned int i = 0; i < ker->numberOfPoints; ++i){
        tempsaveXpoint = ker->points[i]->point[0];
        tempsaveYpoint = ker->points[i]->point[1];
        tempsaveZpoint = ker->points[i]->point[2];

        ker->points[i]->point[0] = rotation3D_matrix[0][0] * tempsaveXpoint + rotation3D_matrix[0][1] * tempsaveYpoint + rotation3D_matrix[0][2] * tempsaveZpoint;
        ker->points[i]->point[1] = rotation3D_matrix[1][0] * tempsaveXpoint + rotation3D_matrix[1][1] * tempsaveYpoint + rotation3D_matrix[1][2] * tempsaveZpoint;
        ker->points[i]->point[2] = rotation3D_matrix[2][0] * tempsaveXpoint + rotation3D_matrix[2][1] * tempsaveYpoint + rotation3D_matrix[2][2] * tempsaveZpoint;
       
    }
/*
    for(unsigned int i=0; i < k; ++i){
        printf("(%.16Lf %.16Lf %.16Lf)\n", ker->points[i]->point[0], ker->points[i]->point[1], ker->points[i]->point[2]);
    }
*/
    return ker;
}

kernel* horizontal_negative45Deg_LineKernel_lidarSpaced(unsigned int k, long double range_in_meter, LidarFeatures* ptr){
    if(k % 2 == 0){
        --k;
    }
    kernel* ker = (kernel*)malloc(sizeof(kernel));
    ker->numberOfPoints = k;
    ker->points = (point_3D**)calloc(k, sizeof(point_3D*));
    for(unsigned int i=0; i < k; ++i){
        ker->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }
    ker->points[k/2]->point[0] = 0.;
    ker->points[k/2]->point[1] = 0.;
    ker->points[k/2]->point[2] = 0.;
    long double spaceing_y, spaceing_x, prevSpace_x = 0.0, prevSpace_y = 0.0, z;
        for(unsigned int i = 1; i <= k/2 ; ++i){
        z = range_in_meter * tanl(ptr->horizontal_angle*((long double)i));
        spaceing_x = z*sinl(45) - prevSpace_x;
        spaceing_y = z*cosl(45) - prevSpace_y;
        ker->points[k/2-i]->point[0] = (-1.0)*(fabsl(ker->points[k/2-i+1]->point[1]) + spaceing_y - prevSpace_y);
        ker->points[k/2-i]->point[1] = (-1.0)*(fabsl(ker->points[k/2-i+1]->point[0]) + spaceing_x - prevSpace_x);
        ker->points[k/2-i]->point[2] = ker->points[k/2-i+1]->point[2];

        ker->points[k/2+i]->point[0] = ker->points[k/2+i-1]->point[0] + spaceing_y - prevSpace_y;
        ker->points[k/2+i]->point[1] = ker->points[k/2+i-1]->point[1] + spaceing_x - prevSpace_x;
        ker->points[k/2+i]->point[2] = ker->points[k/2+i-1]->point[2];
        prevSpace_y = spaceing_y;
        prevSpace_x = spaceing_x;
    }

    for(unsigned int i=0; i < k; ++i){
        printf("(%.16Lf %.16Lf %.16Lf)\n", ker->points[i]->point[0], ker->points[i]->point[1], ker->points[i]->point[2]);
    }
    return ker;
}



kernel* horizontal_cylinder_leveled_kernel_eqSpaced(unsigned int k, long double c, long double spacing, long double theta, long double level_spacing, int level){
    kernel* ker1 = horizontal_cylinder_kernel_eqSpaced(k, c, spacing, theta);
    kernel* ker2 = horizontal_cylinder_kernel_eqSpaced(k, c, spacing, theta);
    
    kernel* ker = (kernel*)malloc(sizeof(kernel));
    
    ker->numberOfPoints = k*level;
    ker->points = (point_3D**)calloc(k*level, sizeof(point_3D*));
    for(unsigned int i=0; i < k*level; ++i){
        ker->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }

    for(unsigned int i=0; i < k; ++i){
        copyPoints(ker->points[i], ker1->points[i]);
        if( level != 1 ){
            ker2->points[i]->point[2] = level_spacing;
            copyPoints(ker->points[k+i], ker2->points[i]);
        }
    }
    
    for(unsigned int i=0; i < k*level; ++i){
       // printf("(%.16Lf %.16Lf %.16Lf)\n", ker->points[i]->point[0], ker->points[i]->point[1], ker->points[i]->point[2]);
    }
    return ker;
}



kernel* sphere_kernel_eqSpaced(unsigned int k, unsigned int z, long double c, long double spacing, long double theta, long double level_spacing){
    kernel* ker1 = horizontal_cylinder_kernel_eqSpaced(k, c, spacing, theta);
    kernel* ker2 = vertical_cylinder_kernel_eqSpaced(z, c, level_spacing, theta);
    
    kernel* ker = (kernel*)malloc(sizeof(kernel));
    
    ker->numberOfPoints = k+z;
    ker->points = (point_3D**)calloc(ker->numberOfPoints, sizeof(point_3D*));
    for(unsigned int i=0; i < ker->numberOfPoints; ++i){
        ker->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }

    for(unsigned int i=0; i < k; ++i){
        copyPoints(ker->points[i], ker1->points[i]);
        free(ker1->points[i]);
    }
    for(unsigned int i=0; i < z; ++i){
        copyPoints(ker->points[k+i], ker2->points[i]);
        free(ker2->points[i]);
    }
    for(unsigned int i=0; i < ker->numberOfPoints; ++i){
       // printf("(%.16Lf %.16Lf %.16Lf)\n", ker->points[i]->point[0], ker->points[i]->point[1], ker->points[i]->point[2]);
    }
    //free(ker1);
    //free(ker2);
    return ker;
}

kernel* vertical_cylinder_kernel_eqSpaced(unsigned int k, long double c, long double spacing, long double theta){
    if(k % 2 == 0){
        --k;
    }
    kernel* ker = (kernel*)malloc(sizeof(kernel));
    ker->numberOfPoints = k;
    ker->points = (point_3D**)calloc(k, sizeof(point_3D*));
    for(unsigned int i=0; i < k; ++i){
        ker->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }

    ker->points[k/2]->point[0] = 0.; // x
    ker->points[k/2]->point[1] = cylinderEquation(0.,c); // y
    ker->points[k/2]->point[2] = 0.; // z

    for(unsigned int i = 1; i <= k/2 ; ++i){
        ker->points[k/2-i]->point[2] = -(spacing *(long double)i);
        ker->points[k/2-i]->point[1] = cylinderEquation(-(spacing *(long double)i),c);
        ker->points[k/2-i]->point[0] = 0;

        ker->points[k/2+i]->point[2] = spacing *(long double)i;
        ker->points[k/2+i]->point[1] = cylinderEquation(spacing *(long double)i,c);
        ker->points[k/2+i]->point[0] = 0;
    }

    long double rotateM[2][2] = {
        {cosl(theta),-sinl(theta)},
        {sinl(theta),cosl(theta)}
    };
   // printf("\n========== Rotational Matrix =========\n");
   // printf("|%.16Lf | %.16Lf|\n|%.16Lf | %.16Lf|\n", rotateM[0][0], rotateM[0][1], rotateM[1][0], rotateM[1][1]);
    long double tempsaveXpoint, tempsaveYpoint;
    for(unsigned int i = 0; i < k ; ++i){
        tempsaveXpoint = ker->points[i]->point[0];
        tempsaveYpoint = ker->points[i]->point[1];
        ker->points[i]->point[0] = rotateM[0][0] * tempsaveXpoint + rotateM[0][1] * tempsaveYpoint;
        ker->points[i]->point[1] = rotateM[1][0] * tempsaveXpoint + rotateM[1][1] * tempsaveYpoint;
        ker->points[i]->point[2] = ker->points[i]->point[2];
    }

    for(unsigned int i=0; i < k; ++i){
        //printf("(%.16Lf %.16Lf %.16Lf)\n", ker->points[i]->point[0], ker->points[i]->point[1], ker->points[i]->point[2]);
    }
    return ker;
}






kernel* horizontal_cylinder_kernel_eqSpaced(unsigned int k, long double c, long double spacing, long double theta){
    if(k % 2 == 0){
        --k;
    }
    kernel* ker = (kernel*)malloc(sizeof(kernel));
    ker->numberOfPoints = k;
    ker->points = (point_3D**)calloc(k, sizeof(point_3D*));
    for(unsigned int i=0; i < k; ++i){
        ker->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }

    ker->points[k/2]->point[0] = 0.; // x
    ker->points[k/2]->point[1] = cylinderEquation(0.,c); // y
    ker->points[k/2]->point[2] = 0.; // z

    for(unsigned int i = 1; i <= k/2 ; ++i){
        ker->points[k/2-i]->point[0] = -(spacing *(long double)i);
        ker->points[k/2-i]->point[1] = cylinderEquation(-(spacing *(long double)i),c);
        ker->points[k/2-i]->point[2] = 0;

        ker->points[k/2+i]->point[0] = spacing *(long double)i;
        ker->points[k/2+i]->point[1] = cylinderEquation(spacing *(long double)i,c);
        ker->points[k/2+i]->point[2] = 0;
    }

    long double rotateM[2][2] = {
        {cosl(theta),-sinl(theta)},
        {sinl(theta),cosl(theta)}
    };
   // printf("\n========== Rotational Matrix =========\n");
   // printf("|%.16Lf | %.16Lf|\n|%.16Lf | %.16Lf|\n", rotateM[0][0], rotateM[0][1], rotateM[1][0], rotateM[1][1]);
    long double tempsaveXpoint, tempsaveYpoint;
    for(unsigned int i = 0; i < k ; ++i){
        tempsaveXpoint = ker->points[i]->point[0];
        tempsaveYpoint = ker->points[i]->point[1];
        ker->points[i]->point[0] = rotateM[0][0] * tempsaveXpoint + rotateM[0][1] * tempsaveYpoint;
        ker->points[i]->point[1] = rotateM[1][0] * tempsaveXpoint + rotateM[1][1] * tempsaveYpoint;
        ker->points[i]->point[2] = ker->points[i]->point[2];
    }

    for(unsigned int i=0; i < k; ++i){
        //printf("(%.16Lf %.16Lf %.16Lf)\n", ker->points[i]->point[0], ker->points[i]->point[1], ker->points[i]->point[2]);
    }
    return ker;
}

long double cylinderEquation(long double x, long double c){
    return sqrtl(1.-x*x + c) - sqrtl(1. + c) ;
}


kernel * verticalLineKernel(unsigned int k, long double spaceing){
    //make k ODD
    if(k % 2 == 0){
        --k;
    }
    kernel* ker = (kernel*)malloc(sizeof(kernel));
    ker->numberOfPoints = k;
    ker->points = (point_3D**)calloc(k, sizeof(point_3D*));
    for(unsigned int i=0; i < k; ++i){
        ker->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }
    ker->points[k/2]->point[0] = 0.;
    ker->points[k/2]->point[1] = 0.;
    ker->points[k/2]->point[2] = 0.;
    for(unsigned int i = 1; i <= k/2 ; ++i){
        ker->points[k/2-i]->point[0] = ker->points[k/2-i+1]->point[0];
        ker->points[k/2-i]->point[1] = ker->points[k/2-i+1]->point[1];
        ker->points[k/2-i]->point[2] = (-1.0)*(fabsl(ker->points[k/2-i+1]->point[2]) + spaceing);

        ker->points[k/2+i]->point[0] = ker->points[k/2+i-1]->point[0];
        ker->points[k/2+i]->point[1] = ker->points[k/2+i-1]->point[1];
        ker->points[k/2+i]->point[2] = ker->points[k/2+i-1]->point[2] + spaceing;
    }

    for(unsigned int i=0; i < k; ++i){
        printf("(%.16Lf %.16Lf %.16Lf), ", ker->points[i]->point[0], ker->points[i]->point[1], ker->points[i]->point[2]);
    }
    return ker;
}

kernel* square_horizontal(unsigned int k, long double spaceing){
    if(k % 2 == 0){
        --k;
    }
    kernel* ker = (kernel*)malloc(sizeof(kernel));
    ker->points = (point_3D**)calloc(k*k, sizeof(point_3D*));
        for(unsigned int i=0; i < k*k; ++i){
        ker->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }
    ker->numberOfPoints = k*k;

    point_3D** object3D = (point_3D**)calloc(k, sizeof(point_3D*));
        
    for(unsigned int i=0; i < k; ++i){
        object3D[i] = (point_3D*)calloc(k, sizeof(point_3D));
    }
    object3D[k/2][k/2].point[0] = 0.0;
    object3D[k/2][k/2].point[1] = 0.0;
    object3D[k/2][k/2].point[2] = 0.0;
    // Init Vertical, Horizontal Cross

        
    for(unsigned int i=1; i <= k/2; ++i){
            // Horizontal, right
            object3D[k/2][k/2 + i].point[0] = object3D[k/2][k/2 + i - 1].point[0]; //x
            object3D[k/2][k/2 + i].point[1] = object3D[k/2][k/2 + i - 1].point[1] - spaceing; //y
            object3D[k/2][k/2 + i].point[2] = object3D[k/2][k/2 + i - 1].point[2]; //z
            // Horizontal, left
            object3D[k/2][k/2 - i].point[0] = object3D[k/2][k/2 - i + 1].point[0]; //x
            object3D[k/2][k/2 - i].point[1] = object3D[k/2][k/2 - i + 1].point[1] + spaceing; //y
            object3D[k/2][k/2 - i].point[2] = object3D[k/2][k/2 - i + 1].point[2]; //z

            // Vertical, downWard
            object3D[k/2 + i][k/2].point[0] = object3D[k/2 + i - 1][k/2].point[0] - spaceing; //x
            object3D[k/2 + i][k/2].point[1] = object3D[k/2 + i - 1][k/2].point[1]; //y
            object3D[k/2 + i][k/2].point[2] = object3D[k/2 + i - 1][k/2].point[2]; //z
            // Vertical, uppward
            object3D[k/2 - i][k/2].point[0] = object3D[k/2 - i + 1][k/2].point[0] + spaceing; //x
            object3D[k/2 - i][k/2].point[1] = object3D[k/2 - i + 1][k/2].point[1]; //y
            object3D[k/2 - i][k/2].point[2] = object3D[k/2 - i + 1][k/2].point[2]; //z;

    }
    

    for(unsigned int i=1; i <= k/2; ++i){
            for(unsigned int j=1; j <= k/2; ++j){
            // lower Left matrix part
            object3D[k/2 + i][k/2 - j].point[0] = object3D[k/2 + i - 1][k/2 - j].point[0] + object3D[k/2 + i][k/2 + 1 - j].point[0];
            object3D[k/2 + i][k/2 - j].point[1] = object3D[k/2 + i - 1][k/2 - j].point[1] + object3D[k/2 + i][k/2 + 1 - j].point[1];
            object3D[k/2 + i][k/2 - j].point[2] = object3D[k/2 + i - 1][k/2 - j].point[2] + object3D[k/2 + i][k/2 + 1 - j].point[2];
            // lower Right matrix part
            object3D[k/2 + i][k/2 + j].point[0] = object3D[k/2 + i - 1][k/2 + j].point[0] + object3D[k/2 + i][k/2 - 1 + j].point[0];
            object3D[k/2 + i][k/2 + j].point[1] = object3D[k/2 + i - 1][k/2 + j].point[1] + object3D[k/2 + i][k/2 - 1 + j].point[1];
            object3D[k/2 + i][k/2 + j].point[2] = object3D[k/2 + i - 1][k/2 + j].point[2] + object3D[k/2 + i][k/2 - 1 + j].point[2];
            // Upper Right matrix part
            object3D[k/2 - i][k/2 + j].point[0] = object3D[k/2 - i + 1][k/2 + j].point[0] + object3D[k/2 - i][k/2 - 1 + j].point[0];
            object3D[k/2 - i][k/2 + j].point[1] = object3D[k/2 - i + 1][k/2 + j].point[1] + object3D[k/2 - i][k/2 - 1 + j].point[1];
            object3D[k/2 - i][k/2 + j].point[2] = object3D[k/2 - i + 1][k/2 + j].point[2] + object3D[k/2 - i][k/2 - 1 + j].point[2];
            // Upper Left matrix part
            object3D[k/2 - i][k/2 - j].point[0] = object3D[k/2 - i + 1][k/2 - j].point[0] + object3D[k/2 - i][k/2 + 1 - j].point[0];
            object3D[k/2 - i][k/2 - j].point[1] = object3D[k/2 - i + 1][k/2 - j].point[1] + object3D[k/2 - i][k/2 + 1 - j].point[1];
            object3D[k/2 - i][k/2 - j].point[2] = object3D[k/2 - i + 1][k/2 - j].point[2] + object3D[k/2 - i][k/2 + 1 - j].point[2];

        }

    }


    if(1){
        for(unsigned int i = 0; i < k; ++i){
            for(unsigned int j = 0; j < k; ++j){
                printf("(%.2Lf, %.2Lf, %.2Lf) ", object3D[i][j].point[0],  object3D[i][j].point[1], object3D[i][j].point[2]);
            }
            printf("\n");
        }
    }
    unsigned int pointCount = 0;
    for(unsigned int i = 0; i < k; ++i){
        for(unsigned int j = 0; j < k; ++j){
            copyPoints( ker->points[pointCount] , &object3D[i][j]);
            pointCount++;
        }
        free(object3D[i]);
    }
    free(object3D);
    
    return ker;

}

kernel* square_Vertical_along_Y(unsigned int k, long double spaceing){
    if(k % 2 == 0){
        --k;
    }
    kernel* ker = (kernel*)malloc(sizeof(kernel));
    ker->points = (point_3D**)calloc(k*k, sizeof(point_3D*));
        for(unsigned int i=0; i < k*k; ++i){
        ker->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }
    ker->numberOfPoints = k*k;

    point_3D** object3D = (point_3D**)calloc(k, sizeof(point_3D*));
        
    for(unsigned int i=0; i < k; ++i){
        object3D[i] = (point_3D*)calloc(k, sizeof(point_3D));
    }
    object3D[k/2][k/2].point[0] = 0.0;
    object3D[k/2][k/2].point[1] = 0.0;
    object3D[k/2][k/2].point[2] = 0.0;
    // Init Vertical, Horizontal Cross

        
    for(unsigned int i=1; i <= k/2; ++i){
            // Horizontal, right
            object3D[k/2][k/2 + i].point[0] = object3D[k/2][k/2 + i - 1].point[0]; //x
            object3D[k/2][k/2 + i].point[1] = object3D[k/2][k/2 + i - 1].point[1] - spaceing; //y
            object3D[k/2][k/2 + i].point[2] = object3D[k/2][k/2 + i - 1].point[2]; //z
            // Horizontal, left
            object3D[k/2][k/2 - i].point[0] = object3D[k/2][k/2 - i + 1].point[0]; //x
            object3D[k/2][k/2 - i].point[1] = object3D[k/2][k/2 - i + 1].point[1] + spaceing; //y
            object3D[k/2][k/2 - i].point[2] = object3D[k/2][k/2 - i + 1].point[2]; //z

            // Vertical, downWard
            object3D[k/2 + i][k/2].point[0] = object3D[k/2 + i - 1][k/2].point[0]; //x
            object3D[k/2 + i][k/2].point[1] = object3D[k/2 + i - 1][k/2].point[1]; //y
            object3D[k/2 + i][k/2].point[2] = object3D[k/2 + i - 1][k/2].point[2] - spaceing; //z
            // Vertical, uppward
            object3D[k/2 - i][k/2].point[0] = object3D[k/2 - i + 1][k/2].point[0]; //x
            object3D[k/2 - i][k/2].point[1] = object3D[k/2 - i + 1][k/2].point[1]; //y
            object3D[k/2 - i][k/2].point[2] = object3D[k/2 - i + 1][k/2].point[2] + spaceing; //z;

    }
    

    for(unsigned int i=1; i <= k/2; ++i){
            for(unsigned int j=1; j <= k/2; ++j){
            // lower Left matrix part
            object3D[k/2 + i][k/2 - j].point[0] = object3D[k/2 + i - 1][k/2 - j].point[0] + object3D[k/2 + i][k/2 + 1 - j].point[0];
            object3D[k/2 + i][k/2 - j].point[1] = object3D[k/2 + i - 1][k/2 - j].point[1] + object3D[k/2 + i][k/2 + 1 - j].point[1];
            object3D[k/2 + i][k/2 - j].point[2] = object3D[k/2 + i - 1][k/2 - j].point[2] + object3D[k/2 + i][k/2 + 1 - j].point[2];
            // lower Right matrix part
            object3D[k/2 + i][k/2 + j].point[0] = object3D[k/2 + i - 1][k/2 + j].point[0] + object3D[k/2 + i][k/2 - 1 + j].point[0];
            object3D[k/2 + i][k/2 + j].point[1] = object3D[k/2 + i - 1][k/2 + j].point[1] + object3D[k/2 + i][k/2 - 1 + j].point[1];
            object3D[k/2 + i][k/2 + j].point[2] = object3D[k/2 + i - 1][k/2 + j].point[2] + object3D[k/2 + i][k/2 - 1 + j].point[2];
            // Upper Right matrix part
            object3D[k/2 - i][k/2 + j].point[0] = object3D[k/2 - i + 1][k/2 + j].point[0] + object3D[k/2 - i][k/2 - 1 + j].point[0];
            object3D[k/2 - i][k/2 + j].point[1] = object3D[k/2 - i + 1][k/2 + j].point[1] + object3D[k/2 - i][k/2 - 1 + j].point[1];
            object3D[k/2 - i][k/2 + j].point[2] = object3D[k/2 - i + 1][k/2 + j].point[2] + object3D[k/2 - i][k/2 - 1 + j].point[2];
            // Upper Left matrix part
            object3D[k/2 - i][k/2 - j].point[0] = object3D[k/2 - i + 1][k/2 - j].point[0] + object3D[k/2 - i][k/2 + 1 - j].point[0];
            object3D[k/2 - i][k/2 - j].point[1] = object3D[k/2 - i + 1][k/2 - j].point[1] + object3D[k/2 - i][k/2 + 1 - j].point[1];
            object3D[k/2 - i][k/2 - j].point[2] = object3D[k/2 - i + 1][k/2 - j].point[2] + object3D[k/2 - i][k/2 + 1 - j].point[2];

        }

    }


    if(1){
        for(unsigned int i = 0; i < k; ++i){
            for(unsigned int j = 0; j < k; ++j){
                printf("(%.2Lf, %.2Lf, %.2Lf) ", object3D[i][j].point[0],  object3D[i][j].point[1], object3D[i][j].point[2]);
            }
            printf("\n");
        }
    }
    unsigned int pointCount = 0;
    for(unsigned int i = 0; i < k; ++i){
        for(unsigned int j = 0; j < k; ++j){
            copyPoints( ker->points[pointCount] , &object3D[i][j]);
            pointCount++;
        }
        free(object3D[i]);
    }
    free(object3D);
    
    return ker;

}

kernel* square_Vertical_along_X(unsigned int k, long double  spaceing){
    if(k % 2 == 0){
        --k;
    }
    kernel* ker = (kernel*)malloc(sizeof(kernel));
    ker->points = (point_3D**)calloc(k*k, sizeof(point_3D*));
        for(unsigned int i=0; i < k*k; ++i){
        ker->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }
    ker->numberOfPoints = k*k;

    point_3D** object3D = (point_3D**)calloc(k, sizeof(point_3D*));
        
    for(unsigned int i=0; i < k; ++i){
        object3D[i] = (point_3D*)calloc(k, sizeof(point_3D));
    }
    object3D[k/2][k/2].point[0] = 0.0;
    object3D[k/2][k/2].point[1] = 0.0;
    object3D[k/2][k/2].point[2] = 0.0;
    // Init Vertical, Horizontal Cross

        
    for(unsigned int i=1; i <= k/2; ++i){
            // Horizontal, right
            object3D[k/2][k/2 + i].point[0] = object3D[k/2][k/2 + i - 1].point[0] - spaceing; //x
            object3D[k/2][k/2 + i].point[1] = object3D[k/2][k/2 + i - 1].point[1]; //y
            object3D[k/2][k/2 + i].point[2] = object3D[k/2][k/2 + i - 1].point[2]; //z
            // Horizontal, left
            object3D[k/2][k/2 - i].point[0] = object3D[k/2][k/2 - i + 1].point[0] + spaceing; //x
            object3D[k/2][k/2 - i].point[1] = object3D[k/2][k/2 - i + 1].point[1]; //y
            object3D[k/2][k/2 - i].point[2] = object3D[k/2][k/2 - i + 1].point[2]; //z

            // Vertical, downWard
            object3D[k/2 + i][k/2].point[0] = object3D[k/2 + i - 1][k/2].point[0]; //x
            object3D[k/2 + i][k/2].point[1] = object3D[k/2 + i - 1][k/2].point[1]; //y
            object3D[k/2 + i][k/2].point[2] = object3D[k/2 + i - 1][k/2].point[2] - spaceing; //
            // Vertical, uppward
            object3D[k/2 - i][k/2].point[0] = object3D[k/2 - i + 1][k/2].point[0]; //x
            object3D[k/2 - i][k/2].point[1] = object3D[k/2 - i + 1][k/2].point[1]; //y
            object3D[k/2 - i][k/2].point[2] = object3D[k/2 - i + 1][k/2].point[2] + spaceing; //z;

    }
    

    for(unsigned int i=1; i <= k/2; ++i){
            for(unsigned int j=1; j <= k/2; ++j){
            // lower Left matrix part
            object3D[k/2 + i][k/2 - j].point[0] = object3D[k/2 + i - 1][k/2 - j].point[0] + object3D[k/2 + i][k/2 + 1 - j].point[0];
            object3D[k/2 + i][k/2 - j].point[1] = object3D[k/2 + i - 1][k/2 - j].point[1] + object3D[k/2 + i][k/2 + 1 - j].point[1];
            object3D[k/2 + i][k/2 - j].point[2] = object3D[k/2 + i - 1][k/2 - j].point[2] + object3D[k/2 + i][k/2 + 1 - j].point[2];
            // lower Right matrix part
            object3D[k/2 + i][k/2 + j].point[0] = object3D[k/2 + i - 1][k/2 + j].point[0] + object3D[k/2 + i][k/2 - 1 + j].point[0];
            object3D[k/2 + i][k/2 + j].point[1] = object3D[k/2 + i - 1][k/2 + j].point[1] + object3D[k/2 + i][k/2 - 1 + j].point[1];
            object3D[k/2 + i][k/2 + j].point[2] = object3D[k/2 + i - 1][k/2 + j].point[2] + object3D[k/2 + i][k/2 - 1 + j].point[2];
            // Upper Right matrix part
            object3D[k/2 - i][k/2 + j].point[0] = object3D[k/2 - i + 1][k/2 + j].point[0] + object3D[k/2 - i][k/2 - 1 + j].point[0];
            object3D[k/2 - i][k/2 + j].point[1] = object3D[k/2 - i + 1][k/2 + j].point[1] + object3D[k/2 - i][k/2 - 1 + j].point[1];
            object3D[k/2 - i][k/2 + j].point[2] = object3D[k/2 - i + 1][k/2 + j].point[2] + object3D[k/2 - i][k/2 - 1 + j].point[2];
            // Upper Left matrix part
            object3D[k/2 - i][k/2 - j].point[0] = object3D[k/2 - i + 1][k/2 - j].point[0] + object3D[k/2 - i][k/2 + 1 - j].point[0];
            object3D[k/2 - i][k/2 - j].point[1] = object3D[k/2 - i + 1][k/2 - j].point[1] + object3D[k/2 - i][k/2 + 1 - j].point[1];
            object3D[k/2 - i][k/2 - j].point[2] = object3D[k/2 - i + 1][k/2 - j].point[2] + object3D[k/2 - i][k/2 + 1 - j].point[2];

        }

    }


    if(1){
        for(unsigned int i = 0; i < k; ++i){
            for(unsigned int j = 0; j < k; ++j){
                printf("(%.2Lf, %.2Lf, %.2Lf) ", object3D[i][j].point[0],  object3D[i][j].point[1], object3D[i][j].point[2]);
            }
            printf("\n");
        }
    }
    unsigned int pointCount = 0;
    for(unsigned int i = 0; i < k; ++i){
        for(unsigned int j = 0; j < k; ++j){
            copyPoints( ker->points[pointCount] , &object3D[i][j]);
            pointCount++;
        }
        free(object3D[i]);
    }
    free(object3D);
    
    return ker;

}


kernel* cubeKernel(unsigned int k, long double spaceing){
    //ODD DIM(k)
    if(k % 2 == 0){
        --k;
    }
    kernel* ker = (kernel*)malloc(sizeof(kernel)); 
    ker->points = (point_3D**)calloc(k*k*k, sizeof(point_3D*));
    for(unsigned int i=0; i < k*k*k; ++i){
        ker->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }
    ker->numberOfPoints = k*k*k;
    //generate temporary 3D object

    point_3D*** object3D = (point_3D***)calloc(k, sizeof(point_3D**));

    for(unsigned int i=0; i < k; ++i){
        object3D[i] = (point_3D**)calloc(k, sizeof(point_3D*));
        
        for(unsigned int j=0; j < k; ++j){
            object3D[i][j] = (point_3D*)calloc(k, sizeof(point_3D));
        }
    }

    // middle slice of 3D object (2D object)
    // Init Origo
    object3D[k/2][k/2][k/2].point[0] = 0.0;
    object3D[k/2][k/2][k/2].point[1] = 0.0;
    object3D[k/2][k/2][k/2].point[2] = 0.0;

    for(unsigned int dim=1; dim <= k/2; ++dim){
        object3D[k/2 - dim][k/2][k/2].point[0] = object3D[k/2 - dim + 1][k/2][k/2].point[0];
        object3D[k/2 - dim][k/2][k/2].point[1] = object3D[k/2 - dim + 1][k/2][k/2].point[1] - spaceing;
        object3D[k/2 - dim][k/2][k/2].point[2] = object3D[k/2 - dim + 1][k/2][k/2].point[2];

        object3D[k/2 - dim][k/2][k/2].point[0] = object3D[k/2 - dim + 1][k/2][k/2].point[0];
        object3D[k/2 + dim][k/2][k/2].point[1] = object3D[k/2 + dim - 1][k/2][k/2].point[1] + spaceing;
        object3D[k/2 - dim][k/2][k/2].point[2] = object3D[k/2 - dim + 1][k/2][k/2].point[2];
    }
    
    // Init Vertical, Horizontal Cross
    for(unsigned int dim = 0; dim < k; ++dim){
        
        for(unsigned int i=1; i <= k/2; ++i){
            // Horizontal, right
            object3D[dim][k/2][k/2 + i].point[0] = object3D[dim][k/2][k/2 + i - 1].point[0] - spaceing; //x
            object3D[dim][k/2][k/2 + i].point[1] = object3D[dim][k/2][k/2 + i - 1].point[1]; //y
            object3D[dim][k/2][k/2 + i].point[2] = object3D[dim][k/2][k/2 + i - 1].point[2]; //z
            // Horizontal, left
            object3D[dim][k/2][k/2 - i].point[0] = object3D[dim][k/2][k/2 - i + 1].point[0] + spaceing; //x
            object3D[dim][k/2][k/2 - i].point[1] = object3D[dim][k/2][k/2 - i + 1].point[1]; //y
            object3D[dim][k/2][k/2 - i].point[2] = object3D[dim][k/2][k/2 - i + 1].point[2]; //z

            // Vertical, downWard
            object3D[dim][k/2 + i][k/2].point[0] = object3D[dim][k/2 + i - 1][k/2].point[0]; //x
            object3D[dim][k/2 + i][k/2].point[1] = object3D[dim][k/2 + i - 1][k/2].point[1]; //y
            object3D[dim][k/2 + i][k/2].point[2] = object3D[dim][k/2 + i - 1][k/2].point[2] - spaceing; //
            // Vertical, uppward
            object3D[dim][k/2 - i][k/2].point[0] = object3D[dim][k/2 - i + 1][k/2].point[0]; //x
            object3D[dim][k/2 - i][k/2].point[1] = object3D[dim][k/2 - i + 1][k/2].point[1]; //y
            object3D[dim][k/2 - i][k/2].point[2] = object3D[dim][k/2 - i + 1][k/2].point[2] + spaceing; //z;
        }
    }

    
    //FILL UP 2D object
    for(unsigned int dim = 0; dim < k; ++dim){
        for(unsigned int i=1; i <= k/2; ++i){
            for(unsigned int j=1; j <= k/2; ++j){
            // lower Left matrix part
            object3D[dim][k/2 + i][k/2 - j].point[0] = object3D[dim][k/2 + i - 1][k/2 - j].point[0] + object3D[dim][k/2 + i][k/2 + 1 - j].point[0];
            object3D[dim][k/2 + i][k/2 - j].point[1] = object3D[dim][k/2 + i - 1][k/2 - j].point[1] + object3D[dim][k/2 + i][k/2 + 1 - j].point[1];
            object3D[dim][k/2 + i][k/2 - j].point[2] = object3D[dim][k/2 + i - 1][k/2 - j].point[2] + object3D[dim][k/2 + i][k/2 + 1 - j].point[2];
            // lower Right matrix part
            object3D[dim][k/2 + i][k/2 + j].point[0] = object3D[dim][k/2 + i - 1][k/2 + j].point[0] + object3D[dim][k/2 + i][k/2 - 1 + j].point[0];
            object3D[dim][k/2 + i][k/2 + j].point[1] = object3D[dim][k/2 + i - 1][k/2 + j].point[1] + object3D[dim][k/2 + i][k/2 - 1 + j].point[1];
            object3D[dim][k/2 + i][k/2 + j].point[2] = object3D[dim][k/2 + i - 1][k/2 + j].point[2] + object3D[dim][k/2 + i][k/2 - 1 + j].point[2];
            // Upper Right matrix part
            object3D[dim][k/2 - i][k/2 + j].point[0] = object3D[dim][k/2 - i + 1][k/2 + j].point[0] + object3D[dim][k/2 - i][k/2 - 1 + j].point[0];
            object3D[dim][k/2 - i][k/2 + j].point[1] = object3D[dim][k/2 - i + 1][k/2 + j].point[1] + object3D[dim][k/2 - i][k/2 - 1 + j].point[1];
            object3D[dim][k/2 - i][k/2 + j].point[2] = object3D[dim][k/2 - i + 1][k/2 + j].point[2] + object3D[dim][k/2 - i][k/2 - 1 + j].point[2];
            // Upper Left matrix part
            object3D[dim][k/2 - i][k/2 - j].point[0] = object3D[dim][k/2 - i + 1][k/2 - j].point[0] + object3D[dim][k/2 - i][k/2 + 1 - j].point[0];
            object3D[dim][k/2 - i][k/2 - j].point[1] = object3D[dim][k/2 - i + 1][k/2 - j].point[1] + object3D[dim][k/2 - i][k/2 + 1 - j].point[1];
            object3D[dim][k/2 - i][k/2 - j].point[2] = object3D[dim][k/2 - i + 1][k/2 - j].point[2] + object3D[dim][k/2 - i][k/2 + 1 - j].point[2];

        }
        
    }    
    }

    
    if(0){
    for(unsigned int dim = 0; dim < k; ++dim){
        for(unsigned int i = 0; i < k; ++i){
            for(unsigned int j = 0; j < k; ++j){
                printf("(%.2Lf, %.2Lf, %.2Lf) ", object3D[dim][i][j].point[0],  object3D[dim][i][j].point[1], object3D[dim][i][j].point[2]);
            }
            printf("\n");
        }
         printf("\n\n");
    }

    }
    unsigned int pointCount = 0;
    for(unsigned int dim = 0; dim < k; ++dim){
        for(unsigned int i = 0; i < k; ++i){
            for(unsigned int j = 0; j < k; ++j){
                copyPoints( ker->points[pointCount] , &object3D[dim][i][j]);
                pointCount++;
            }
            free(object3D[dim][i]);
        }
        free(object3D[dim]);
    }

    free(object3D);
    return ker;
}


points_3D_set* visualizeKernel(kernel* (kernelFunc)(unsigned int, long double, LidarFeatures*  ), unsigned int k, LidarFeatures* ptr){
    points_3D_set* points3D = (points_3D_set*)malloc(sizeof(points_3D_set));
    points3D->unionTreshold = UNIONTRESHOLD;
    int num = 0, currentPoint = 0;
    for(long double i = 0; i < 5.0; i+=0.3){
        num++;
    }
    points3D->numberOfPoints = num*k;

    points3D->points = (point_3D**) calloc(points3D->numberOfPoints, sizeof(point_3D*));
    for(unsigned int i = 0; i < points3D->numberOfPoints; i++){
        points3D->points[i] = (point_3D*)malloc(sizeof(point_3D));
    }

    point_3D tempP;
    
    for(long double i = 1.0000000000000000; i < 5.0; i+=1.000000000000000){
        kernel* ker = kernelFunc(k, i, ptr);
        for(int k =0; k < ker->numberOfPoints; ++k){
            tempP.point[0] = ker->points[k]->point[0];
            tempP.point[1] = ker->points[k]->point[1]+i;
            tempP.point[2] = ker->points[k]->point[2];
            copyPoints(points3D->points[currentPoint], &tempP);
            currentPoint++;
        }
    }
    points3D->numberOfPoints = currentPoint;
    return points3D;
}


// ================================ DATA STRUCTURE ================================
MatrixTree* createMatrixTree(points_3D_set* pointset, int dimX, int dimY){
    MatrixTree* matrixTree = (MatrixTree*)malloc(sizeof(MatrixTree));
    int min_X = pointset->points[0]->point[0] , min_Y =  pointset->points[0]->point[1], max_X =  pointset->points[0]->point[0], max_Y = pointset->points[0]->point[1];
    for(unsigned int i = 1; i < pointset->numberOfPoints ; ++i){
        if( pointset->points[i]->point[0] < min_X){
            min_X = pointset->points[i]->point[0];
        }
        if( pointset->points[i]->point[1] < min_Y){
            min_Y = pointset->points[i]->point[1];
        }

        if( pointset->points[i]->point[0] > max_X){
            max_X = pointset->points[i]->point[0];
        }
        if( pointset->points[i]->point[1] > max_Y){
            max_Y = pointset->points[i]->point[1];
        }
    }
    matrixTree->min_X = min_X;
    matrixTree->min_Y = min_Y;
    matrixTree->max_X = max_X;
    matrixTree->max_Y = max_Y;
    matrixTree->dimX = dimX;
    matrixTree->dimY = dimY;
    matrixTree->space_X = ((matrixTree->max_X - matrixTree->min_X) / (long double) matrixTree->dimX);
    matrixTree->space_Y = ((matrixTree->max_Y - matrixTree->min_Y) / (long double) matrixTree->dimY);
    matrixTree->matrixValue = (Node***)malloc(sizeof(Node**) * matrixTree->dimX);
    matrixTree->matrixElementSize = (int**)malloc(sizeof(int*) * matrixTree->dimX);

    for(unsigned int i= 0; i < matrixTree->dimX; ++i){
        matrixTree->matrixElementSize[i] = (int*)calloc(matrixTree->dimY, sizeof(int));
    }

    for(unsigned int i= 0; i < matrixTree->dimX; ++i){
        matrixTree->matrixValue[i] = (Node**)malloc(sizeof(Node**) * matrixTree->dimY);
    }
    for(unsigned int i= 0; i < matrixTree->dimX; ++i){
        for(unsigned int j= 0; j < matrixTree->dimY; ++j){
            matrixTree->matrixValue[i][j] = (Node*)malloc(sizeof(Node));
            matrixTree->matrixValue[i][j]->point = (point_3D*)malloc(sizeof(point_3D));
            matrixTree->matrixValue[i][j]->point->point[0] = 0. ;
            matrixTree->matrixValue[i][j]->point->point[1] = 0. ;
            matrixTree->matrixValue[i][j]->point->point[2] = 0. ;
            matrixTree->matrixValue[i][j]->x = NULL;
            matrixTree->matrixValue[i][j]->y = NULL;
            matrixTree->matrixValue[i][j]->z = NULL;
            matrixTree->matrixElementSize[i][j] +=1;
        }
    }
/*
    for(unsigned int i= 0; i < matrixTree->dimX; ++i){
        for(unsigned int j= 0; j < matrixTree->dimY; ++j){
            printf("(%.2Lf %.2Lf %.2Lf) ", matrixTree->matrixValue[i][j]->point->point[0], matrixTree->matrixValue[i][j]->point->point[1], matrixTree->matrixValue[i][j]->point->point[2]);
        }
        printf("\n");
    }

    unsigned index_X, index_Y;
    for(index_X = 0; index_X < matrixTree->dimX; ++index_X){
        printf(" %i: %.4Lf" , index_X, matrixTree->space_X * (long double)index_X  + matrixTree->min_X);

    }
    printf("\n");
    for(index_Y = 0; index_Y < matrixTree->dimX; ++index_Y){
        printf(" %i: %.4Lf" , index_Y, matrixTree->space_Y * (long double)index_Y  + matrixTree->min_Y);

    }
*/
    // ==================== INSERT ELEMENTS TO MATRIX TREE =================
    for(unsigned int i = 0; i < pointset->numberOfPoints ; ++i){
        unsigned index_X, index_Y;
        for(index_X = 1; index_X < matrixTree->dimX; ++index_X){
            if( pointset->points[i]->point[0] < matrixTree->space_X * (long double)index_X  + matrixTree->min_X){
                break;
            }
        }
        for(index_Y = 1; index_Y < matrixTree->dimY; ++index_Y){

            if( pointset->points[i]->point[1] < matrixTree->space_Y * (long double)index_Y  + matrixTree->min_Y){        
                break;
            }

        }
       // printf("%i %i\n", index_X-1, index_Y-1);
        //printf("(%.2Lf %.2Lf %.2Lf) ", matrixTree->matrixValue[0][0]->point->point[0], matrixTree->matrixValue[0][0]->point->point[1], matrixTree->matrixValue[0][0]->point->point[2]);
        if(insertTreeNode(matrixTree->matrixValue[index_X-1][index_Y-1], pointset->points[i], pointset->unionTreshold) != -1){
           matrixTree->matrixElementSize[index_X-1][index_Y-1] += 1;
        }
      
        //long double test = pointset->points[i]->point[0] / matrixTree->dimX ;
    }
    return matrixTree;
}

MatrixTree* createMatrixTree_empty(int dimX, int dimY, long double min_X, long double min_Y, long double max_X, long double max_Y){
    MatrixTree* matrixTree = (MatrixTree*)malloc(sizeof(MatrixTree));
    matrixTree->min_X = min_X;
    matrixTree->min_Y = min_Y;
    matrixTree->max_X = max_X;
    matrixTree->max_Y = max_Y;
    matrixTree->dimX = dimX;
    matrixTree->dimY = dimY;
    matrixTree->space_X = ((matrixTree->max_X - matrixTree->min_X) / (long double) matrixTree->dimX);
    matrixTree->space_Y = ((matrixTree->max_Y - matrixTree->min_Y) / (long double) matrixTree->dimY);
    matrixTree->matrixValue = (Node***)malloc(sizeof(Node**) * matrixTree->dimX);
    matrixTree->matrixElementSize = (int**)malloc(sizeof(int*) * matrixTree->dimX);

    for(unsigned int i= 0; i < matrixTree->dimX; ++i){
        matrixTree->matrixElementSize[i] = (int*)calloc(matrixTree->dimY, sizeof(int));
    }

    for(unsigned int i= 0; i < matrixTree->dimX; ++i){
        matrixTree->matrixValue[i] = (Node**)malloc(sizeof(Node**) * matrixTree->dimY);
    }
    for(unsigned int i= 0; i < matrixTree->dimX; ++i){
        for(unsigned int j= 0; j < matrixTree->dimY; ++j){
            matrixTree->matrixValue[i][j] = (Node*)malloc(sizeof(Node));
            matrixTree->matrixValue[i][j]->point = (point_3D*)malloc(sizeof(point_3D));
            matrixTree->matrixValue[i][j]->point->point[0] = 0. ;
            matrixTree->matrixValue[i][j]->point->point[1] = 0. ;
            matrixTree->matrixValue[i][j]->point->point[2] = 0. ;
            matrixTree->matrixValue[i][j]->x = NULL;
            matrixTree->matrixValue[i][j]->y = NULL;
            matrixTree->matrixValue[i][j]->z = NULL;
            matrixTree->matrixElementSize[i][j] +=1;
        }
    }
    return matrixTree;
}


Node* createBinaryTree(points_3D_set* points3D){
    Node* head = (Node*)malloc(sizeof(Node));
    head->point = (point_3D*)malloc(sizeof(point_3D));
    head->point->point[0] = points3D->points[0]->point[0] ;
    head->point->point[1] = points3D->points[0]->point[1] ;
    head->point->point[2] = points3D->points[0]->point[2] ;
    head->x = NULL;
    head->y = NULL;
    head->z = NULL;
    for(unsigned int i = 1; i < points3D->numberOfPoints ; ++i){
        //printf("%d %d\n",insertTreeNode(head, points3D->points[i], points3D->unionTreshold), i);
        insertTreeNode(head, points3D->points[i], points3D->unionTreshold);
        //fprintf(stdout, "%.16Lf %.16Lf %.16Lf \n", points3D->points[i]->point[0], points3D->points[i]->point[1], points3D->points[i]->point[2]);
    }
    return head;
}

Node* createBinaryTree_empty(){
    Node* head = (Node*)malloc(sizeof(Node));
    head->point = (point_3D*)malloc(sizeof(point_3D));
    head->point->point[0] = 0. ;
    head->point->point[1] = 0. ;
    head->point->point[2] = 0.;
    head->x = NULL;
    head->y = NULL;
    head->z = NULL;
    return head;
}


//search along the X axis, if there is no data within that treshold, create a new node of X
// if there is, search through that X axis for Y values, if it not exist create a node if yes then
// search along the Y axis for Z value etc....
int insertTreeNode(Node* head, point_3D* point, long double threshold){
    Node* newNode = head, *prevNode = head;
    int prevIndicator = 0;
    // traverse Z axis
    while(newNode != NULL){
        if(fabsl(point->point[2] - newNode->point->point[2]) > threshold){
            if(prevIndicator != 0){
                prevNode = prevNode->z;
            }
            newNode = newNode->z;
            prevIndicator = 1;
        } else{
            break;
        }
    }
    if( newNode == NULL){
        Node* newNode = (Node*)malloc(sizeof(Node));
        newNode->point = (point_3D*)malloc(sizeof(point_3D));
        copyPoints(newNode->point, point);
        newNode->x = NULL;
        newNode->y = NULL;
        newNode->z = NULL;
        prevNode->z = newNode;
        return 3;
    }
    prevIndicator = 0;
    prevNode = newNode;
    // traverse X axis
    while(newNode != NULL){
        if(fabsl(point->point[0] - newNode->point->point[0] ) > threshold){
            if(prevIndicator != 0){
                prevNode = prevNode->x;
            }
            newNode = newNode->x;
            prevIndicator = 1;
        } else{
            break;
        }
    }
    if( newNode == NULL){
        
        Node* newNode = (Node*)malloc(sizeof(Node));
        newNode->point = (point_3D*)malloc(sizeof(point_3D));
        copyPoints(newNode->point, point);
        //fprintf(stdout, "%.16Lf %.16Lf %.16Lf \n", newNode->point[0], newNode->point[1], newNode->point[2]);
        //fprintf(stdout, "%.16Lf %.16Lf %.16Lf \n", point[0], point[1], point[2]);
        newNode->x = NULL;
        newNode->y = NULL;
        newNode->z = NULL;
        prevNode->x = newNode;
        return 1;
    }
    prevIndicator = 0;
    prevNode = newNode;
    // traverse Y axis
    while(newNode != NULL){
        if(fabsl(point->point[1] - newNode->point->point[1]) > threshold){
            if(prevIndicator != 0){
                prevNode = prevNode->y;
            }
            newNode = newNode->y;
            prevIndicator = 1;
        } else{
            break;
        }
    }
    if( newNode == NULL){
        Node* newNode = (Node*)malloc(sizeof(Node));
        newNode->point = (point_3D*)malloc(sizeof(point_3D));
        copyPoints(newNode->point, point);
        newNode->x = NULL;
        newNode->y = NULL;
        newNode->z = NULL;
        prevNode->y = newNode;
        return 2;
    }
    return -1;
}

// the problem is that if the threshold is bigger then the spacing between axis-es, then it will
//find the FIRST point on that axis where the if statement is true and seach within that context
//but this is wrong since it can fail, if the searched node is in the next spaced element
void searchTree_Zaxis(point_3D* searchPoint, Node* nextNode, long double threshold,  int* found){
    if(nextNode != NULL){
        if(fabsl(nextNode->point->point[2] - searchPoint->point[2]) < threshold){
            searchTree_Xaxis(searchPoint, nextNode->x, threshold, found);
        }
        searchTree_Zaxis(searchPoint, nextNode->z, threshold, found);
    }
}

void searchTree_Xaxis(point_3D* searchPoint, Node* nextNode, long double threshold,  int* found){
    if(nextNode != NULL){
        if(fabsl(nextNode->point->point[0] - searchPoint->point[0]) < threshold){
            searchTree_Yaxis(searchPoint, nextNode->y, threshold, found);
        }
        searchTree_Xaxis(searchPoint, nextNode->x, threshold, found);
    }
}

void searchTree_Yaxis(point_3D* searchPoint, Node* nextNode, long double threshold, int* found){
    if(nextNode != NULL){
        if(fabsl(nextNode->point->point[1] - searchPoint->point[1]) < threshold){
            *found = 1;
        }
        searchTree_Yaxis(searchPoint, nextNode->y, threshold, found);
    }
}

int searchTree_Rec(point_3D* searchPoint, Node* head, long double threshold){
    Node* nextNode = head;
    int found = -1;
    searchTree_Zaxis(searchPoint, nextNode, threshold, &found);
    return found;
}

int searchTree(point_3D* searchPoint, Node* head, long double threshold){
    Node* nextNode = head;
    int indicator = 0;
    //search Z axis
    while(nextNode != NULL){
        if(fabsl(nextNode->point->point[2] - searchPoint->point[2]) < threshold){
            indicator = 1;
            break;
        }
        nextNode = nextNode->z;
    }
    if(indicator == 0){
        return -1;
    }
    //search X axis
    indicator = 0;
    while(nextNode != NULL){
        if(fabsl(nextNode->point->point[0] - searchPoint->point[0]) < threshold){
            indicator = 1;
            break;
        }
        nextNode = nextNode->x;
    }
    if(indicator == 0){
        return -1;
    }
    //search Y axis
    indicator = 0;
    while(nextNode != NULL){
        if(fabsl(nextNode->point->point[1] - searchPoint->point[1]) < threshold){
            indicator = 1;
            break;
        }
        nextNode = nextNode->y;
    }
    if(indicator == 0){
        return -1;
    }
//    printf("point - x:%.16Lf y:%.16Lf z:%.16Lf\n node - x:%.16Lf y:%.16Lf z:%.16Lf\n\n", searchPoint->point[0], searchPoint->point[1], searchPoint->point[2], nextNode->point->point[0], nextNode->point->point[1], nextNode->point->point[2]);

    return 1;
}