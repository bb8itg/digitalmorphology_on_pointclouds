
/*
#include <iostream>
#include <thread>
#include <mutex>

std::mutex mtx; // Mutex for synchronization

void recursive_function(int n) {
    if (n <= 0)
        return;

    // Print the current recursion level
    {
        std::lock_guard<std::mutex> lock(mtx);
        std::cout << "Recursion level " << n << std::endl;
    }

    // Recursive call in a new thread
    std::thread t(recursive_function, n - 1);
    t.join(); // Wait for the thread to finish
}

int main() {
    // Start the recursive function in a thread
    std::thread t(recursive_function, 5);
    t.join(); // Wait for the thread to finish

    return 0;
}
*/