# include "test.h"
#include <time.h>


void TEST_dilation_tree(){
    points_3D_set* pointset = handleXYZ_files("pointCloud/floor2.xyz");
    pointset->unionTreshold = 0.12;
    Node* head = createBinaryTree(pointset);
    
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);
    printLidarFeatures(returnLidar);
    for(long double i = 0; i < 5.0; i+=0.3){
        //printf("%.1Lf meter spacing %.16Lf\n", i, spacing_by_Lidar_features(returnLidar, i));
    }
    kernel* line_hY_adaptive = horizontal_Y_LineKernel_lidarSpaced(5, 0.7, returnLidar);
    Node* dilatedY = dilate_Tree(head, line_hY_adaptive, pointset->unionTreshold-0.01);
    saveXYZ_files_Tree("results/TEST_tree_dilated.xyz", dilatedY);
    saveXYZ_files_Tree("results/TEST_REFORMAT_tree_dilated.xyz", head);
    printf("\n ==================== TEST DONE ====================\n");
}

void TEST_dilation_matrix_tree(){
    printf("\n===== START: TEST_dilation_matrix_tree ====\n");
    clock_t start, end;
    double cpu_time_used;
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);
    printLidarFeatures(returnLidar);
    for(long double i = 0; i < 5.0; i+=0.3){
        //printf("%.1Lf meter spacing %.16Lf\n", i, spacing_by_Lidar_features(returnLidar, i));
    }
    kernel* line_hY_adaptive = horizontal_Y_LineKernel_lidarSpaced(5, 0.7, returnLidar);
    // ==================== CREATING MATRIX TREE ===================
    points_3D_set* pointset = handleXYZ_files("pointCloud/floor2.xyz");
    pointset->unionTreshold = 0.1;
    MatrixTree* matrixTree = createMatrixTree(pointset, 8, 8);
    MatrixTree* newMatrixTree = createMatrixTree_empty(matrixTree->dimX, matrixTree->dimY, matrixTree->min_X, matrixTree->min_Y, matrixTree->max_X, matrixTree->max_Y);
    dilate_Matrix_Tree(matrixTree, newMatrixTree, line_hY_adaptive, 0.0000001);
    printf("\n tree size by func\n");
    int sum = 0;
    for(int i = 0; i < matrixTree->dimX; ++i){
        for(int j = 0; j < matrixTree->dimY; ++j){
            int val = sizeOfTree(matrixTree->matrixValue[i][j]);
            printf("%i      ", val);
            sum += val;
        }
        printf("\n");
    }
    printf("sum: %i\n", sum);
    printf("\n tree size by func\n");
    sum = 0;
    for(int i = 0; i < newMatrixTree->dimX; ++i){
        for(int j = 0; j < newMatrixTree->dimY; ++j){
            int val = sizeOfTree(newMatrixTree->matrixValue[i][j]);
            printf("%i      ", val);
            sum += val;
        }
        printf("\n");
    }
    printf("sum: %i\n", sum);
    Node* unifiedTree = createBinaryTree_empty();

    for(int z = 0; z < newMatrixTree->dimX; ++z){
        for(int j = 0; j < newMatrixTree->dimY; ++j){
            unifiedTree = union_Tree(unifiedTree,  newMatrixTree->matrixValue[z][j], 1.0E-15);
        }
    }
    saveXYZ_files_Tree("results/unifiedTree.xyz", unifiedTree);
    printf("\n ==================== TEST DONE ====================\n");   
}

void TEST_detect_X_Y_planes_arrayData(){
    double cpu_time_used;
    clock_t start, end;
    printf("\n\n");
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);
    printLidarFeatures(returnLidar);
    for(long double i = 0; i < 5.0; i+=0.3){
        //printf("%.1Lf meter spacing %.16Lf\n", i, spacing_by_Lidar_features(returnLidar, i));
    }
    kernel* line_hX_adaptive = horizontal_X_LineKernel_lidarSpaced(5, 0.3, returnLidar);
    kernel* line_hY_adaptive = horizontal_Y_LineKernel_lidarSpaced(5, 0.3, returnLidar);
    points_3D_set* planes_hx_p_adaptive = handleXYZ_files("pointCloud/allInOne.xyz");
    points_3D_set* planes_hy_p_adaptive = handleXYZ_files("pointCloud/allInOne.xyz");
    printf("\n\nx:%d y:%d\n\n", planes_hx_p_adaptive->numberOfPoints, planes_hx_p_adaptive->numberOfPoints);
    printf("\n\n%d\n\n", planes_hy_p_adaptive->numberOfPoints);
    start = clock();
    erode(planes_hx_p_adaptive, line_hX_adaptive, (long double) 0.07);
    erode(planes_hy_p_adaptive, line_hY_adaptive, (long double) 0.07);
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    // Convert CPU time to milliseconds
    double milliseconds = cpu_time_used * 1000;

    printf("Time taken: %f milliseconds\n", milliseconds);

    saveXYZ_files("results/TEST_planes_hX_p_ADAPTIVE.xyz",planes_hx_p_adaptive);
    saveXYZ_files("results/TEST_planes_hY_p_ADAPTIVE.xyz",planes_hy_p_adaptive);
    printf("\n ==================== TEST DONE ====================\n");
}

void TEST_detect_45_deg_rotated_planes_arrayData(){
    // ============== 45 deg planes ==============
    printf("\n\n");
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);
    printLidarFeatures(returnLidar);
    for(long double i = 0; i < 5.0; i+=0.3){
        //printf("%.1Lf meter spacing %.16Lf\n", i, spacing_by_Lidar_features(returnLidar, i));
    }
    kernel* line_hX_adaptive = horizontal_X_LineKernel_lidarSpaced(5, 0.3, returnLidar);
    printf("\n\n");
    kernel* line_45Deg_adaptive = horizontal_positive45Deg_LineKernel_lidarSpaced(7, 0.3, returnLidar);
    printf("\n\n");
    //poorly defined FIX THIS NEG 45 DEG
    kernel* line_neg45Deg_adaptive = horizontal_negative45Deg_LineKernel_lidarSpaced(7, 0.3, returnLidar);
    
    points_3D_set* planes_45_adaptive = handleXYZ_files("pointCloud/floor2.xyz");
    //points_3D_set* planes_neg45_adaptive = handleXYZ_files("pointCloud/floor2.xyz");

    erode(planes_45_adaptive, line_45Deg_adaptive, (long double) 0.15);
    //erode(planes_neg45_adaptive, line_neg45Deg_adaptive, (long double) 0.15);
    saveXYZ_files("results/TEST_planes_45deg.xyz",planes_45_adaptive);
   // saveXYZ_files("results/TEST_planes_neg45deg.xyz",planes_neg45_adaptive);
    printf("\n ==================== TEST DONE ====================\n");
}


void TEST_detect_cylinders_arrayData(){
    // ============== DETECT CYLINDER ==============
    printf("\n\n");
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);
    kernel* ker_cyl = horizontal_cylinder_kernel_eqSpaced(7, -0.9, 0.07, -1.60);
    //points_3D_set* cylinders = handleXYZ_files("pointCloud/floor2.xyz");
    points_3D_set* cylinders = handleXYZ_files("pointCloud/basement_columns_noplane.xyz");
    erode(cylinders, ker_cyl, (long double) 0.25);
    saveXYZ_files("results/TEST_cylinders.xyz",cylinders);
    saveXYZ_kernel_asFile("results/cylinderXaxis.xyz", ker_cyl); 
    printf("\n ==================== TEST DONE ====================\n");
}

void TEST_detect_X_Y_45_deg_planes_treeData(){
    // ============== BINARY TREE STRUCT ==============
    points_3D_set* pointset = handleXYZ_files("pointCloud/floor2.xyz");
    pointset->unionTreshold = 0.12;
    Node* head = createBinaryTree(pointset);
    
    //kernel* line_45Deg_adaptive = horizontal_positive45Deg_LineKernel_lidarSpaced(7, 0.3, returnLidar);
    //Node* newhead;
    //kernel* line_hX = horizontal_X_LineKernel_eqSpaced(5, pointset->unionTreshold);
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);
    printLidarFeatures(returnLidar);
    for(long double i = 0; i < 5.0; i+=0.3){
        //printf("%.1Lf meter spacing %.16Lf\n", i, spacing_by_Lidar_features(returnLidar, i));
    }
    kernel* line_hY_adaptive = horizontal_Y_LineKernel_lidarSpaced(5, 0.3, returnLidar);
    kernel* line_hX_adaptive = horizontal_X_LineKernel_lidarSpaced(5, 0.3, returnLidar);
    kernel* line45_adaptive = horizontal_positive45Deg_LineKernel_lidarSpaced(5, 0.3, returnLidar);
    //kernel* line_hX = square_Vertical_along_X(3, pointset->unionTreshold);
    //kernel* line_hY = square_Vertical_along_Y(5, pointset->unionTreshold);
    Node* erodedX = erode_Tree(head, line_hX_adaptive, pointset->unionTreshold-0.01);
    Node* erodedY = erode_Tree(head, line_hY_adaptive, pointset->unionTreshold-0.01);
    Node* eroded45 = erode_Tree(head, line45_adaptive, pointset->unionTreshold-0.01);
    printf("%p %p %p\n", erodedX->z, erodedX->x, erodedX->y);
    saveXYZ_files_Tree("results/TEST_floor1_BIN_X.xyz", erodedX);
    saveXYZ_files_Tree("results/TEST_floor1_BIN_Y.xyz", erodedY);
    saveXYZ_files_Tree("results/TEST_floor1_BIN_Eroded.xyz", eroded45);
    saveXYZ_files_Tree("results/TEST_REFORMAT_floorBinary.xyz", head);
    printf("\n ==================== TEST DONE ====================\n");
}

void TEST_downsample_pointcloud_treeData(){
    // ============== ALL PLANES ==============
    kernel* kerArray[20];
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);


    points_3D_set* pointset = handleXYZ_files("pointCloud/floor2.xyz");
    pointset->unionTreshold = 0.12;
    Node* head = createBinaryTree(pointset);
    char filename[] = "results/ker_0_DEG.xyz";
    saveXYZ_files_Tree(filename, head);

    printf("\n ==================== TEST DONE ====================\n");   
}

void TEST_detect_all_planes_treeData(){
    // ============== ALL PLANES ==============
    double cpu_time_used;
    clock_t start, end;
    kernel* kerArray[20];
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);

    points_3D_set* pointset = handleXYZ_files("pointCloud/floor2.xyz");
    pointset->unionTreshold = 0.12;
    Node* head = createBinaryTree(pointset);
    char filename[] = "results/ker_0_DEG.xyz";
    start = clock();
    for(int i =0; i < 20; i++){
        kerArray[i] = horizontal_positiveDeg_LineKernel_lidarSpaced(5, 0.3, 30.*(long double) i, returnLidar);
        filename[12] = (char) i + 'a' ;
        saveXYZ_files_Tree(filename, erode_Tree(head, kerArray[i], pointset->unionTreshold));
    }
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    // Convert CPU time to milliseconds
    double milliseconds = cpu_time_used * 1000;

    printf("Time taken: %f milliseconds\n", milliseconds);
    printf("\n ==================== TEST DONE ====================\n");   
}

void TEST_detect_cylinders_treeData(){
    fclose(stdout);
    kernel* kerArray[30];
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);


    points_3D_set* pointset = handleXYZ_files("pointCloud/basement_columns.xyz");
    pointset->unionTreshold = 0.15;
    Node* head = createBinaryTree(pointset);
    char filename[128];
    /*
    for(int i =0; i < 12; i++){
        kerArray[i]  =  horizontal_cylinder_kernel_eqSpaced(7, -0.8, 0.13, 1.*(long double) i);
        filename[12] = (char) i + 'a' ;
        saveXYZ_files_Tree(filename, erode_Tree(head, kerArray[i], pointset->unionTreshold+0.15));
    }
    */
   saveXYZ_files_Tree("results/ORIGINAL.xyz", head);
    Node* unifiedTree = createBinaryTree_empty();
    int filenameIndex = 0;
    for(long double j =0.; j < 0.4; j+=0.1){
        for(int i =0; i < 20; i++){
            kerArray[i]  =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.9+j, 0.10 + j/2., 1.*(long double) i, 0.4, 2);
            unifiedTree = union_Tree(unifiedTree, erode_Tree(head, kerArray[i], pointset->unionTreshold+0.1) , 0.1);
       
            //sprintf(filename,"results/ker_%d_DEG.xyz",filenameIndex);
            //saveXYZ_files_Tree(filename, erode_Tree(head, kerArray[i], pointset->unionTreshold+0.1));
            ++filenameIndex;
        }
    
    }
    saveXYZ_files_Tree("results/unified.xyz", unifiedTree);
    printf("\n ==================== TEST DONE ====================\n");

}


void TEST_detect_cylinders_fineTuned_treeData(){
    fclose(stdout);
    kernel* kerArray[1024];
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);


    points_3D_set* pointset = handleXYZ_files("pointCloud/basement.xyz");
    pointset->unionTreshold = 0.05;
    Node* head = createBinaryTree(pointset);
    char filename[128];
    /*
    for(int i =0; i < 12; i++){
        kerArray[i]  =  horizontal_cylinder_kernel_eqSpaced(7, -0.8, 0.13, 1.*(long double) i);
        filename[12] = (char) i + 'a' ;
        saveXYZ_files_Tree(filename, erode_Tree(head, kerArray[i], pointset->unionTreshold+0.15));
    }
    */
   saveXYZ_files_Tree("results/ORIGINAL.xyz", head);
    Node* unifiedTree = createBinaryTree_empty();
    int filenameIndex = 0;
    for(long double j =0.0; j < 0.15; j+=0.005){
        for(int i =0; i < 45; i++){
            kerArray[i]  =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99+j, j*1.5, 1.*(long double) i, 0.4, 2);
            unifiedTree = union_Tree(unifiedTree, erode_Tree(head, kerArray[i], 0.185) , pointset->unionTreshold);
       
            //sprintf(filename,"results/ker_%d_DEG.xyz",filenameIndex);
            //saveXYZ_files_Tree(filename, erode_Tree(head, kerArray[i], pointset->unionTreshold+0.1));
            ++filenameIndex;
        }
    
    }
    saveXYZ_files_Tree("results/unified.xyz", unifiedTree);
}

void TEST_detect_cylinders_handTailored_treeData(){
    kernel* ker;
    // =========== FOR DETECTING ALL CYLINDERS ===========
    char filename[128];;
    kernel* kernels[4084];
    int kerCount = 0;
    long double ringLayer = 0.0;
    long double levelSpacing = 0.0;
    points_3D_set* pointset = handleXYZ_files("pointCloud/basement.xyz");
    pointset->unionTreshold = 0.05;
    Node* head = createBinaryTree(pointset);

    saveXYZ_files_Tree("results/ORIGINAL.xyz", head);
    double cpu_time_used;
    clock_t start, end;
    start = clock();
    Node* unifiedTree = createBinaryTree_empty();
    ringLayer = 0.05;
    for(long double i = 0.; i < 100.; i+= 1.5){
        kernels[kerCount] =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99 + ringLayer, 0.10, 0.6-i, levelSpacing, 1);
        unifiedTree = union_Tree(unifiedTree, erode_Tree(head, kernels[kerCount], 0.02) , pointset->unionTreshold);
        ++kerCount;
    }
    
    ringLayer = 0.06;
    for(long double i = 0.; i < 100.; i+= 1.5){
        kernels[kerCount] =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99 + ringLayer, 0.11, 0.6-i, levelSpacing, 1);
        unifiedTree = union_Tree(unifiedTree, erode_Tree(head, kernels[kerCount], 0.03) , pointset->unionTreshold);
        ++kerCount;
    }
    ringLayer = 0.07;
    for(long double i = 0.; i < 100.; i+= 1.5){
        kernels[kerCount] =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99 + ringLayer, 0.12, 0.6-i, levelSpacing, 1);
        unifiedTree = union_Tree(unifiedTree, erode_Tree(head, kernels[kerCount], 0.035) , pointset->unionTreshold);
        ++kerCount;
    }
    
    ringLayer = 0.08;
    for(long double i = 0.; i < 100.; i+= 1.5){
        kernels[kerCount] =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99 + ringLayer, 0.13, 0.6-i, levelSpacing, 1);
        unifiedTree = union_Tree(unifiedTree, erode_Tree(head, kernels[kerCount], 0.04) , pointset->unionTreshold);
        ++kerCount;
    }
    
    ringLayer = 0.09;
    for(long double i = 0.; i < 100.; i+= 1.5){
        kernels[kerCount] =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99 + ringLayer, 0.14, 0.6-i, levelSpacing, 1);
        unifiedTree = union_Tree(unifiedTree, erode_Tree(head, kernels[kerCount], 0.045) , pointset->unionTreshold);
        ++kerCount;
    }
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    // Convert CPU time to milliseconds
    double milliseconds = cpu_time_used * 1000;

    printf("Time taken: %f milliseconds\n", milliseconds);
    //int treeSize = 0;
    //Node* test = dilate_Tree(head, horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99 + ringLayer, 0.14, 0.6, levelSpacing, 2), 0.001);
    //sizeOfTree_iterate(test, &treeSize);
    //printf("head %i dilated %i", treeSize, treeSize);
    //saveXYZ_files_Tree("results/test.xyz", test);
    saveXYZ_files_Tree("results/unifiedTree.xyz", unifiedTree);

    printf("\n ==================== TEST DONE ====================\n");   
}

void TEST_detect_planes_cylinders_treeData(){
    kernel* kerArray[12];
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);

    int filenameIndex = 0;
    points_3D_set* pointset = handleXYZ_files("pointCloud/basement_columns.xyz");
    pointset->unionTreshold = 0.12;
    Node* head = createBinaryTree(pointset);
    char filename[] = "results/ker_0_DEG.xyz";
    for(int i =0; i < 12; i++){
        kerArray[i] = horizontal_positiveDeg_LineKernel_lidarSpaced(7, 0.3, 30.*(long double) i, returnLidar);
        sprintf(filename,"results/ker_%d_DEG.xyz",filenameIndex);
        saveXYZ_files_Tree(filename, erode_Tree(head, kerArray[i], pointset->unionTreshold));
        ++filenameIndex;
    }
    printf("\n ==================== TEST DONE ====================\n");
}

void TEST_union_planes_cylinders_treeData(){
    kernel* kerArray[12];
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);


    points_3D_set* pointset = handleXYZ_files("pointCloud/basement_columns.xyz");
    pointset->unionTreshold = 0.12;
    Node* head = createBinaryTree(pointset);
    Node* unifiedTree = createBinaryTree_empty();
    char filename[] = "results/oneFile.xyz";
    for(int i =0; i < 12; i++){
        kerArray[i] = horizontal_positiveDeg_LineKernel_lidarSpaced(7, 0.3, 30.*(long double) i, returnLidar);
        unifiedTree = union_Tree(unifiedTree, erode_Tree(head, kerArray[i], pointset->unionTreshold-0.01) , 0.12);
        
    }
    
    saveXYZ_files_Tree(filename, unifiedTree);
    printf("\n ==================== TEST DONE ====================\n");
}

void TEST_subtract_planes_treeData(){
    kernel* kerArray[12];
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);


    points_3D_set* pointset = handleXYZ_files("pointCloud/floor1.xyz");
    pointset->unionTreshold = 0.12;
    Node* head = createBinaryTree(pointset);
    Node* unifiedTree = createBinaryTree_empty();
    char filename[128];
    int filenameIndex = 0;
    for(int i = 0; i < 12; i++){
        kerArray[i] =  horizontal_positiveDeg_LineKernel_lidarSpaced(7, 0.3, 30.*(long double) i, returnLidar);
        unifiedTree = union_Tree(unifiedTree, erode_Tree(head, kerArray[i], pointset->unionTreshold) , 0.0000001);
        //sprintf(filename,"results/ker_%d_DEG.xyz",filenameIndex);
        //saveXYZ_files_Tree(filename, unifiedTree);
        filenameIndex++;
    }

    Node* subtractedTree = subtract_Tree(head, unifiedTree, pointset->unionTreshold);
    saveXYZ_files_Tree("results/subtractedPlanes.xyz", subtractedTree);
    saveXYZ_files_Tree("results/unifiedTree.xyz", unifiedTree);
    saveXYZ_files_Tree("results/original.xyz", head);
    printf("\n ==================== TEST DONE ====================\n"); 
}

void TEST_detect_planes_veritcal_kernels_treeData(){
    printf("\n===== START: TEST_detect_planes_veritcal_kernels_treeData ====\n");
    kernel* kerArray[512];
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);
    clock_t start, end;
    double cpu_time_used;

    points_3D_set* pointset = handleXYZ_files("pointCloud/allInOne.xyz");
    pointset->unionTreshold = 0.1;
    
    Node* unifiedTree = createBinaryTree_empty();
    char filename[128];
    int filenameIndex = 0;
    int kerNum = 0;
    for(long double i = 0.; i < 6.; i += 0.3){
        for(long double j =0.; j < 0.31; j+=0.1){
            kerArray[kerNum]  =  rotate3D_LineKernel_eqSpaced(7, 0.2, -3. + i, 0.3-j );
            ++kerNum;
        }
    }
    printf("kerNum %i",kerNum);


    start = clock();
    Node* head = createBinaryTree(pointset);
    for(int i = 0; i < kerNum; i++){
        unifiedTree = union_Tree(unifiedTree, erode_Tree(head, kerArray[i], pointset->unionTreshold+0.05) , 0.0000001);
        //erode_Tree(head, kerArray[i], pointset->unionTreshold+0.05);
        filenameIndex++;
    }
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    // Convert CPU time to milliseconds
    double milliseconds = cpu_time_used * 1000;
    printf("Time taken: %f milliseconds\n", milliseconds);

    Node* subtractedTree = subtract_Tree(head, unifiedTree, pointset->unionTreshold);
    saveXYZ_files_Tree("results/subtractedPlanes.xyz", subtractedTree);
    saveXYZ_files_Tree("results/unifiedTree.xyz", unifiedTree);
    saveXYZ_files_Tree("results/original.xyz", head);
    printf("\n ==================== TEST DONE ====================\n"); 
}




void TEST_create_matrix_treeData(){
    printf("===== START: TEST_detect_planes_veritcal_kernels_treeData ====");
    kernel* kerArray[20];
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);
    // ==================== CREATING MATRIX TREE ===================
    points_3D_set* pointset = handleXYZ_files("pointCloud/basement.xyz");
    pointset->unionTreshold = 0.1;
    MatrixTree* matrixTree = createMatrixTree(pointset, 8, 8);
    printf("\n tree size by func\n");
    int sum = 0;
    for(int i = 0; i < matrixTree->dimX; ++i){
        for(int j = 0; j < matrixTree->dimY; ++j){
            int val = sizeOfTree(matrixTree->matrixValue[i][j]);
            printf("%i      ", val);
            sum += val;
        }
        printf("\n");
    }
    printf("sum: %i\n", sum);

    printf("\n tree size by memory\n");
    sum = 0;
    for(int i = 0; i < matrixTree->dimX; ++i){
        for(int j = 0; j < matrixTree->dimY; ++j){
            int val = sizeOfTree(matrixTree->matrixValue[i][j]);
            printf("%i      ", matrixTree->matrixElementSize[i][j]);
            sum += matrixTree->matrixElementSize[i][j];
        }
        printf("\n");
    }
    printf("sum: %i\n", sum);

    Node* head = createBinaryTree(pointset);
    printf("\none tree data: %i  matrix tree data: %i\n",sizeOfTree(head), sum);
    Node* unifiedTree = createBinaryTree_empty();
    for(int i = 0; i < matrixTree->dimX; ++i){
        for(int j = 0; j < matrixTree->dimY; ++j){
            //unifiedTree = union_Tree(unifiedTree, matrixTree->matrixValue[i][j], 1.0E-15);
        }
    }
    saveXYZ_files_Tree("results/ORIGINAL.xyz", head);
    saveXYZ_files_Tree("results/MATRIX_TREE_FILE.xyz", unifiedTree);
    printf("\n ==================== TEST DONE ====================\n");   
}

void TEST_detect_cylinders_handTailored_matrix_treeData(){
    kernel* ker;
    // =========== FOR DETECTING ALL CYLINDERS ===========
    char filename[128];;
    kernel* kernels[4084];
    int kerCount = 0;
    long double ringLayer = 0.0;
    long double levelSpacing = 0.2;
    int level = 2;
    points_3D_set* pointset = handleXYZ_files("pointCloud/output_with_random_points.xyz");
    pointset->unionTreshold = 0.05;
    Node* original = createBinaryTree(pointset);
    MatrixTree* matrixTree = createMatrixTree(pointset, 8, 8);
    MatrixTree* newMatrixTree = createMatrixTree_empty(matrixTree->dimX, matrixTree->dimY, matrixTree->min_X, matrixTree->min_Y, matrixTree->max_X, matrixTree->max_Y);
    
    for(int z = 0; z < matrixTree->dimX; ++z){
        for(int j = 0; j < matrixTree->dimY; ++j){
            if( newMatrixTree->matrixElementSize[z][j] != 1){
                original = union_Tree(original,  newMatrixTree->matrixValue[z][j], 0.0000001);
            }
        }
    }
    saveXYZ_files_Tree("results/ORIGINAL.xyz", original);
    double cpu_time_used;
    clock_t start, end;
    start = clock();
    Node* unifiedTree = createBinaryTree_empty();
    ringLayer = 0.05;
    for(long double i = 0.; i < 100.; i+= 1.5){
        kernels[kerCount] =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99 + ringLayer, 0.10, 0.6-i, levelSpacing, level);
        erode_Matrix_Tree(matrixTree, newMatrixTree, kernels[kerCount], 0.02);
        ++kerCount;
    }
    ringLayer = 0.06;
    for(long double i = 0.; i < 100.; i+= 1.5){
        kernels[kerCount] =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99 + ringLayer, 0.11, 0.6-i, levelSpacing, level);
        erode_Matrix_Tree(matrixTree, newMatrixTree, kernels[kerCount], 0.03);
        ++kerCount;
    }
    ringLayer = 0.07;
    for(long double i = 0.; i < 100.; i+= 1.5){
        kernels[kerCount] =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99 + ringLayer, 0.12, 0.6-i, levelSpacing, level);
        erode_Matrix_Tree(matrixTree, newMatrixTree, kernels[kerCount], 0.035);
        ++kerCount;
    }
    ringLayer = 0.08;
    for(long double i = 0.; i < 100.; i+= 1.5){
        kernels[kerCount] =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99 + ringLayer, 0.13, 0.6-i, levelSpacing, level);
        erode_Matrix_Tree(matrixTree, newMatrixTree, kernels[kerCount], 0.04);
        ++kerCount;
    }
    ringLayer = 0.09;
    for(long double i = 0.; i < 100.; i+= 1.5){
        kernels[kerCount] =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99 + ringLayer, 0.14, 0.6-i, levelSpacing, level);
        erode_Matrix_Tree(matrixTree, newMatrixTree, kernels[kerCount], 0.045);
        ++kerCount;
    }
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    // Convert CPU time to milliseconds
    double milliseconds = cpu_time_used * 1000;

    printf("Time taken: %f milliseconds\n", milliseconds);
    for(int z = 0; z < matrixTree->dimX; ++z){
        for(int j = 0; j < matrixTree->dimY; ++j){
            if( newMatrixTree->matrixElementSize[z][j] != 1){
                unifiedTree = union_Tree(unifiedTree,  newMatrixTree->matrixValue[z][j], 0.0000001);
            }
        }
    }
    //int treeSize = 0;
    //Node* test = dilate_Tree(head, horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99 + ringLayer, 0.14, 0.6, levelSpacing, 2), 0.001);
    //sizeOfTree_iterate(test, &treeSize);
    //printf("head %i dilated %i", treeSize, treeSize);
    //saveXYZ_files_Tree("results/test.xyz", test);
    saveXYZ_files_Tree("results/unifiedTree.xyz", unifiedTree);

}

void TEST_detect_spheres_matrix_treeData(){
    kernel* kernels[99999];
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);

    double cpu_time_used;
    points_3D_set* pointset = handleXYZ_files("pointCloud/output_with_random_points.xyz");
    pointset->unionTreshold = 0.055;
    Node* head = createBinaryTree(pointset);
    char filename[128];
    clock_t start, end;
    saveXYZ_files_Tree("results/ORIGINAL.xyz", head);
    Node* unifiedTree = createBinaryTree_empty();
    int kerCount = 0;

    MatrixTree* matrixTree = createMatrixTree(pointset, 8, 8);
    MatrixTree* newMatrixTree = createMatrixTree_empty(matrixTree->dimX, matrixTree->dimY, matrixTree->min_X, matrixTree->min_Y, matrixTree->max_X, matrixTree->max_Y);

    start = clock();

    int fileCount = 0;
    /*
    for(long double  j =0.04; j < 0.05; j+=0.01){
        for(long double z = 0.08; z < 0.1; z+=0.01){
            for(int i =0; i < 60; i+=1){
                kernel* ker = sphere_kernel_eqSpaced(5, 3, -0.99+j, z, 1.+ (long double)i, 0.055);
                erode_Matrix_Tree(matrixTree, newMatrixTree, ker, (long double)(2.*z/5.));
            }
        fileCount++;
        }
   }
   */
    for(long double  j =0.04; j < 0.05; j+=0.01){
        for(long double z = 0.08; z < 0.1; z+=0.01){
            for(int i =0; i < 60; i+=1){
                kernel* ker = sphere_kernel_eqSpaced(5, 3, -0.99+j, z, 1.+ (long double)i, 0.2);
                erode_Matrix_Tree(matrixTree, newMatrixTree, ker, (long double)(2.*z/5.));
                kerCount++;
            }
        }
    }

    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    for(int z = 0; z < matrixTree->dimX; ++z){
        for(int j = 0; j < matrixTree->dimY; ++j){
            if( newMatrixTree->matrixElementSize[z][j] != 1){
                unifiedTree = union_Tree(unifiedTree,  newMatrixTree->matrixValue[z][j], 0.0000001);
            }
       }
    }
    // Convert CPU time to milliseconds
    double milliseconds = cpu_time_used * 1000;

    printf("Time taken: %f milliseconds\n", milliseconds);
    saveXYZ_files_Tree("results/unifiedMatrix.xyz", unifiedTree);
    printf("\n ==================== TEST DONE ====================\n");  
}

void TEST_detect_spheres_treeData(){
    //fclose(stdout);
    kernel* kernels[99999];
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);

    double cpu_time_used;
    points_3D_set* pointset = handleXYZ_files("pointCloud/combined_points.xyz");
    pointset->unionTreshold = 0.055;
    Node* head = createBinaryTree(pointset);
    char filename[128];
    clock_t start, end;
    saveXYZ_files_Tree("results/ORIGINAL.xyz", head);
    Node* unifiedTree = createBinaryTree_empty();
    int kerCount = 0;
    start = clock();

    int fileCount = 0;
    for(long double  j =0.04; j < 0.05; j+=0.01){
        for(long double z = 0.08; z < 0.1; z+=0.01){
            for(int i =0; i < 60; i+=1){
                kernel* ker = sphere_kernel_eqSpaced(5, 3, -0.99+j, z, 1.+ (long double)i, 0.055);
                unifiedTree = union_Tree(unifiedTree, erode_Tree(head, ker, (long double)(2.*z/5.)) , pointset->unionTreshold);

            }
        fileCount++;
        }
   }
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    // Convert CPU time to milliseconds
    double milliseconds = cpu_time_used * 1000;

    printf("Time taken: %f milliseconds\n", milliseconds);
    saveXYZ_files_Tree("results/unified.xyz", unifiedTree);
    printf("\n ==================== TEST DONE ====================\n");  
}

void TEST_create_matrix_treeData_floor1(){
    printf("===== START: TEST_detect_planes_veritcal_kernels_treeData ====");
    kernel* kerArray[20];
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);
    // ==================== CREATING MATRIX TREE ===================
    points_3D_set* pointset = handleXYZ_files("pointCloud/floor1.xyz");
    pointset->unionTreshold = 0.1;
    MatrixTree* matrixTree = createMatrixTree(pointset, 8, 8);
    printf("\n tree size by func\n");
    int sum = 0;
    for(int i = 0; i < matrixTree->dimX; ++i){
        for(int j = 0; j < matrixTree->dimY; ++j){
            int val = sizeOfTree(matrixTree->matrixValue[i][j]);
            printf("%i      ", val);
            sum += val;
        }
        printf("\n");
    }
    printf("sum: %i\n", sum);

    printf("\n tree size by memory\n");
    sum = 0;
    for(int i = 0; i < matrixTree->dimX; ++i){
        for(int j = 0; j < matrixTree->dimY; ++j){
            int val = sizeOfTree(matrixTree->matrixValue[i][j]);
            printf("%i      ", matrixTree->matrixElementSize[i][j]);
            sum += matrixTree->matrixElementSize[i][j];
        }
        printf("\n");
    }
    printf("sum: %i\n", sum);

    Node* head = createBinaryTree(pointset);
    printf("\none tree data: %i  matrix tree data: %i\n",sizeOfTree(head), sum);
    Node* unifiedTree = createBinaryTree_empty();
    for(int i = 0; i < matrixTree->dimX; ++i){
        for(int j = 0; j < matrixTree->dimY; ++j){
            //unifiedTree = union_Tree(unifiedTree, matrixTree->matrixValue[i][j], 1.0E-15);
        }
    }
    saveXYZ_files_Tree("results/ORIGINAL.xyz", head);
    saveXYZ_files_Tree("results/MATRIX_TREE_FILE.xyz", unifiedTree);
    printf("\n ==================== TEST DONE ====================\n");   
}


void TEST_detect_all_planes_matrix_treeData(){
    printf("\n===== START: TEST_detect_all_planes_matrix_treeData ====\n");
    clock_t start, end;
    double cpu_time_used;
    kernel* kerArray[512];
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);


    Node* unifiedTree = createBinaryTree_empty();
    int kerNum = 0;
    for(long double i = 0.; i < 6.; i += 0.3){
        for(long double j =0.; j < 0.31; j+=0.1){
            kerArray[kerNum] = rotate3D_LineKernel_eqSpaced(7, 0.2, -3. + i, 0.3-j );
            ++kerNum;
        }
    }
    printf("kerNum %i",kerNum);
    // ==================== CREATING MATRIX TREE ===================
    points_3D_set* pointset = handleXYZ_files("pointCloud/output_with_random_points.xyz");
    //points_3D_set* pointset = handleXYZ_files("pointCloud/floor1.xyz");
    pointset->unionTreshold = 0.1;
    MatrixTree* matrixTree = createMatrixTree(pointset, 8, 8);
    MatrixTree* newMatrixTree = createMatrixTree_empty(matrixTree->dimX, matrixTree->dimY, matrixTree->min_X, matrixTree->min_Y, matrixTree->max_X, matrixTree->max_Y);

    start = clock();
    for(int i =0; i < kerNum; i++){
        //unifiedTree = union_Tree(unifiedTree, erode_Matrix_Tree(matrixTree, kerArray[i], pointset->unionTreshold+0.05) , 0.0000001);
        erode_Matrix_Tree(matrixTree, newMatrixTree, kerArray[i], pointset->unionTreshold+0.05);
    }

    for(int z = 0; z < matrixTree->dimX; ++z){
        for(int j = 0; j < matrixTree->dimY; ++j){
            if( newMatrixTree->matrixElementSize[z][j] != 1){
                unifiedTree = union_Tree(unifiedTree,  newMatrixTree->matrixValue[z][j], 0.0000001);
            }
        }
    }

    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    // Convert CPU time to milliseconds
    double milliseconds = cpu_time_used * 1000;

    printf("Time taken: %f milliseconds\n", milliseconds);
    saveXYZ_files_Tree("results/unifiedTree.xyz", unifiedTree);
    printf("\n ==================== TEST DONE ====================\n");   
}

void TEST_detect_planes_veritcal_kernels_treeData_matrix_floor1(){
    printf("\n===== START: TEST_detect_all_planes_matrix_treeData ====\n");
    clock_t start, end;
    double cpu_time_used;
    kernel* kerArray[512];
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);


    Node* unifiedTree = createBinaryTree_empty();
    int kerNum = 0;
    for(long double i = 0.; i < 6.; i += 0.3){
        for(long double j =0.; j < 0.31; j+=0.1){
            kerArray[kerNum] = rotate3D_LineKernel_eqSpaced(7, 0.2, -3. + i, 0.3-j );
            ++kerNum;
        }
    }
    printf("kerNum %i",kerNum);
    // ==================== CREATING MATRIX TREE ===================
    points_3D_set* pointset = handleXYZ_files("pointCloud/floor1.xyz");
    //points_3D_set* pointset = handleXYZ_files("pointCloud/floor1.xyz");
    pointset->unionTreshold = 0.1;
    MatrixTree* matrixTree = createMatrixTree(pointset, 8, 8);
    MatrixTree* newMatrixTree = createMatrixTree_empty(matrixTree->dimX, matrixTree->dimY, matrixTree->min_X, matrixTree->min_Y, matrixTree->max_X, matrixTree->max_Y);

    start = clock();
    for(int i =0; i < kerNum; i++){
        //unifiedTree = union_Tree(unifiedTree, erode_Matrix_Tree(matrixTree, kerArray[i], pointset->unionTreshold+0.05) , 0.0000001);
        erode_Matrix_Tree(matrixTree, newMatrixTree, kerArray[i], pointset->unionTreshold+0.05);
    }

        for(int z = 0; z < matrixTree->dimX; ++z){
            for(int j = 0; j < matrixTree->dimY; ++j){
                if( newMatrixTree->matrixElementSize[z][j] != 1){
                    unifiedTree = union_Tree(unifiedTree,  newMatrixTree->matrixValue[z][j], 0.0000001);
                }
            }
        }

    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    // Convert CPU time to milliseconds
    double milliseconds = cpu_time_used * 1000;

    printf("Time taken: %f milliseconds\n", milliseconds);
    saveXYZ_files_Tree("results/unifiedTree.xyz", unifiedTree);
    printf("\n ==================== TEST DONE ====================\n");   
}

// ====================== KERNEL VISUALIZATION ======================
void VISUALIZE_horizontal_X_kernel(){
    // ============== 45 deg planes ==============
    printf("\n\n");
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);
    printLidarFeatures(returnLidar);
    saveXYZ_files("results/45DEGker.xyz",visualizeKernel(horizontal_X_LineKernel_lidarSpaced, 7, returnLidar));
}

void VISUALIZE_cylinder_kernel_mini(){
    kernel* ker;
    // ============== CYLINDER ==============
#define ALL_CYLINDERS_VISUALIZE
#ifdef ALL_CYLINDERS_VISUALIZE
    // =========== FOR DETECTING ALL CYLINDERS ===========
    char filename[128];
    int filenameIndex = 0;
    kernel* kernels[4084];
    int kerCount = 0;
    for(long double j =0.0; j < 0.2; j+=0.03){
        for(int i =0; i < 45; i++){
            kernels[kerCount] =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99+j, j, 1.*(long double) i, 0.0, 2);
            ++filenameIndex;
            ++kerCount;
        }
        printf("j: %.16Lf\n", j);
    }
    printf("Generated\n");
    saveXYZ_kernelSet_asFile("results/kernelSet.xyz", kernels,kerCount);
#else
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);
    //ker = horizontal_cylinder_kernel_eqSpaced(5, 4., 1., 45.);
    ker = horizontal_cylinder_kernel_eqSpaced(5, -0.8, 0.13, -1.60);
   // ker = horizontal_cylinder_kernel_eqSpaced(5, -0.9999, 0.0031, 0.);
    saveXYZ_kernel_asFile("results/cylinderXaxis.xyz", ker);
    
#endif
}

void VISUALIZE_cylinder_kernel_handTailored(){
    kernel* ker;
    // =========== FOR DETECTING ALL CYLINDERS ===========
    char filename[128];
    int filenameIndex = 0;
    kernel* kernels[4084];
    int kerCount = 0;
    long double ringLayer = 0.0, levelSpacing = 0.0;
    ringLayer = 0.05;
    for(long double i = 0.; i < 100.; i+= 1.5){
        kernels[kerCount] =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99 + ringLayer, 0.1, 0.6-i, levelSpacing, 1);
        ++filenameIndex;
        ++kerCount;
    }
    
    ringLayer = 0.06;
    for(long double i = 0.; i < 100.; i+= 1.5){
        kernels[kerCount] =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99 + ringLayer, 0.11, 0.6-i, levelSpacing, 1);
        ++kerCount;
    }
    ringLayer = 0.07;
    for(long double i = 0.; i < 100.; i+= 1.5){
        kernels[kerCount] =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99 + ringLayer, 0.12, 0.6-i, levelSpacing, 1);
        ++kerCount;
    }
    
    ringLayer = 0.08;
    for(long double i = 0.; i < 100.; i+= 1.5){
        kernels[kerCount] =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99 + ringLayer, 0.13, 0.6-i, levelSpacing, 1);
        ++kerCount;
    }
    ringLayer = 0.09;
    for(long double i = 0.; i < 100.; i+= 1.5){
        kernels[kerCount] =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99 + ringLayer, 0.14, 0.6-i, levelSpacing, 1);
        ++kerCount;
    }
    printf("Generated\n");
    saveXYZ_kernelSet_asFile("results/kernelSet.xyz", kernels,kerCount);

}

void VISUALIZE_cylinder_kernel(){
    kernel* ker;
    // ============== CYLINDER ==============
#define ALL_CYLINDERS_VISUALIZE
#ifdef ALL_CYLINDERS_VISUALIZE
    // =========== FOR DETECTING ALL CYLINDERS ===========
    char filename[128];
    int filenameIndex = 0;
    kernel* kernels[2048];
    int kerCount = 0;
    for(long double j =0.; j < 0.41; j+=0.02){
        for(int i =0; i < 45; i++){
            //ker  =  horizontal_cylinder_kernel_eqSpaced(5, -0.9+j, 0.10 + j/2., 1.*(long double) i);
            kernels[kerCount] =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.99+j, 0.10 + j/2., 1.*(long double) i, 0.0, 2);
            //ker  =  horizontal_cylinder_leveled_kernel_eqSpaced(5, -0.9+j, 0.10 + j/2., 1.*(long double) i, 0.4, 2);
            //sprintf(filename,"results/ker_%d_DEG.xyz",filenameIndex);
            //saveXYZ_kernel_asFile(filename, ker);
            ++filenameIndex;
            ++kerCount;
        }
        printf("j: %.16Lf\n", j);
    }
    printf("Generated\n");
    saveXYZ_kernelSet_asFile("results/kernelSet.xyz", kernels,kerCount);
#else
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);
    //ker = horizontal_cylinder_kernel_eqSpaced(5, 4., 1., 45.);
    ker = horizontal_cylinder_kernel_eqSpaced(5, -0.8, 0.13, -1.60);
   // ker = horizontal_cylinder_kernel_eqSpaced(5, -0.9999, 0.0031, 0.);
    saveXYZ_kernel_asFile("results/cylinderXaxis.xyz", ker);
    
#endif
}

void VISUALIZE_rotated_vertical_kernel(){
    kernel* ker;
    // ============== CYLINDER ==============
#define ALL_VERTICAL_ROTATE_LINE_KERNEL
#ifdef ALL_VERTICAL_ROTATE_LINE_KERNEL
    // =========== FOR DETECTING ALL CYLINDERS ===========
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);
    char filename[128];
    int filenameIndex = 0;
    for(long double i = 0.; i < 6.; i += 0.3){
        for(long double j =0.; j < 0.31; j+=0.1){
            ker  =  rotate3D_LineKernel_eqSpaced(5, 0.3, -3. + i, 0.3-j);
            sprintf(filename,"results/ker_%d_DEG.xyz",filenameIndex);
            saveXYZ_kernel_asFile(filename, ker);
            ++filenameIndex;
        }
    }
#else
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);
    //ker = horizontal_cylinder_kernel_eqSpaced(5, 4., 1., 45.);
    ker = rotate3D_LineKernel_eqSpaced(5, 0.2, 0, 0.);
   // ker = horizontal_cylinder_kernel_eqSpaced(5, -0.9999, 0.0031, 0.);
    saveXYZ_kernel_asFile("results/cylinderXaxis.xyz", ker);
    
#endif
}

void VISUALIZE_sphere_kernel(){
    kernel* ker;
    // ============== CYLINDER ==============
#define ALL_CYLINDERS_VISUALIZE
#ifdef ALL_CYLINDERS_VISUALIZE
    // =========== FOR DETECTING ALL CYLINDERS ===========
    char filename[1024];
    int filenameIndex = 0;
    kernel* kernels[2048];
    int kerCount = 0;
/*
    for(long double  j =0.04; j < 0.05; j+=0.01){
        for(long double z = 0.08; z < 0.1; z+=0.01){
            for(int i =0; i < 60; i+=1){
                kernels[kerCount] = sphere_kernel_eqSpaced(5, 3, -0.99+j, z, 1.+ (long double)i, 0.055);
                kerCount++;
            }
        }
    }
*/
    for(long double  j =0.04; j < 0.05; j+=0.01){
        for(long double z = 0.08; z < 0.1; z+=0.01){
            for(int i =0; i < 60; i+=1){
                kernels[kerCount] =  sphere_kernel_eqSpaced(5, 3, -0.99+j, z, 1.+ (long double)i, 0.2);
                kerCount++;
            }
        }
    }
    printf("Generated\n");
    saveXYZ_kernelSet_asFile("results/kernelSet.xyz", kernels,kerCount);
#else
    LidarFeatures* returnLidar = createLidarFeatures(14976,16);
    //ker = horizontal_cylinder_kernel_eqSpaced(5, 4., 1., 45.);
    ker = horizontal_cylinder_kernel_eqSpaced(5, -0.8, 0.13, -1.60);
   // ker = horizontal_cylinder_kernel_eqSpaced(5, -0.9999, 0.0031, 0.);
    saveXYZ_kernel_asFile("results/cylinderXaxis.xyz", ker);
    
#endif
}